/*
 * JobOfferDialog Messages
 *
 * This contains all the text for the JobOfferDialog component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.JobOfferDialog';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the JobOfferDialog component!',
  },
});
