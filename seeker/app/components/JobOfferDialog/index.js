/**
 *
 * JobOfferDialog
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

//Material ui
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  dialogContent: {
    padding: theme.spacing(0, 7),
  },
  title: {
    color: 'rgba(0, 0, 0, 0.56)',
  },
  button: {
    float: 'right',
    backgroundColor: '#56CCF2',
    color: '#ffffff',
    marginBottom: theme.spacing(2),
  },
}));

function JobOfferDialog({ data, onClose }) {
  const classes = useStyles();
  return (
    <div>
      {data && (
        <Dialog fullWidth maxWidth="md" open={!!data} onClose={onClose}>
          <DialogTitle id="max-width-dialog-title">{data.name}</DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={6}>
              <Grid item md={12}>
                <Typography variant="body2" className={classes.title}>
                  Planuojamas bruto atlygis
                </Typography>
                <Typography>{data.salaryFrom} Eur / mėn</Typography>
              </Grid>
              <Grid item md={6}>
                <Typography variant="body2" className={classes.title}>
                  Įmonė
                </Typography>
                <Typography>{data.companyName}</Typography>
              </Grid>
              <Grid item md={6}>
                <Typography variant="body2" className={classes.title}>
                  Darbo vieta
                </Typography>
                <Typography>{data.city}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item md={12}>
                <Typography variant="body2" className={classes.title}>
                  Darbo aprašymas
                </Typography>
                <Typography>
                  Dėžių su įvairiais produktais iškrovimas iš krovininių mašinų
                  (dėžių svoriai iki 25 kg.), produkcijos rūšiavimas pagal
                  kodus, paletizavimas, apsukimas plėvele. Būsi apmokintas,
                  dirbsi komandoje! Į darbo vietą veža viešasis transportas.
                  Išbandyk šį darbą ir vėliau derink su savo veikla!
                </Typography>
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item md={6}>
                <Typography variant="body2" className={classes.title}>
                  Darbo laikas
                </Typography>
                <Typography>9:00 - 17:00</Typography>
              </Grid>
              <Grid item md={6}>
                <Typography variant="body2" className={classes.title}>
                  Skelbimas galioja iki
                </Typography>
                <Typography>2020 - 12 - 12</Typography>
              </Grid>
            </Grid>
            <Button variant="contained" className={classes.button}>
              Kandidatuoti
            </Button>
          </DialogContent>
        </Dialog>
      )}
    </div>
  );
}

JobOfferDialog.propTypes = {};

export default JobOfferDialog;
