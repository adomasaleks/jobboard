/*
 * DropZoneServiceForm Messages
 *
 * This contains all the text for the DropZoneServiceForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DropZoneServiceForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DropZoneServiceForm component!',
  },
});
