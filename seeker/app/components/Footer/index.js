/**
 *
 * Footer
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: '#ECC30B',
    paddingTop: 20,
    paddingBottom: 20,
  },
}));

function Footer() {
  const classes = useStyles();
  return (
    <div id="Footer" className={classes.root}>
      <Typography variant="subtitle2" color="textSecondary" align="center">
        © 2020 adresas.lt Saugomos visos teisės.
      </Typography>
    </div>
  );
}

Footer.propTypes = {};

export default Footer;
