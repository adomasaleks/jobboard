/**
 *
 * Header
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
//Material-ui
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import history from 'utils/history';

//Components
import LoginForm from 'containers/LoginForm';
import RegisterForm from 'containers/RegisterForm';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  spacing: {
    marginRight: 100,
  },
  link: {
    color: '#000',
    textDecoration: 'none',
    marginRight: 20,
  },
  button: {
    color: '#ffffff',
    backgroundColor: '#56CCF2',
  },
  registerButton: {
    marginRight: 20,
  },
  authButton: {
    paddingRight: 25,
    paddingLeft: 25,
  },
  registerDialog: {
    width: 750,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function Header({ isAuth }) {
  console.log(isAuth);
  const [openLogin, setOpenLogin] = React.useState(false);
  const [openRegister, setOpenRegister] = React.useState(false);
  const classes = useStyles();
  const mainHeader = (
    <div>
      <AppBar
        className={classes.root}
        position="static"
        style={{ backgroundColor: '#F8F8F8' }}
      >
        <Toolbar>
          <Button>
            <Link to="/latest" className={classes.link}>
              Skelbimai
            </Link>
          </Button>
          <Button
            className={classes.registerButton}
            onClick={() => {
              setOpenRegister(!openRegister);
            }}
          >
            Registracija
          </Button>
          <Button
            variant="contained"
            classes={{ contained: classes.button }}
            onClick={() => {
              setOpenLogin(!openLogin);
            }}
          >
            Prisijungti
          </Button>
        </Toolbar>
      </AppBar>
      <Dialog
        open={openLogin}
        TransitionComponent={Transition}
        maxWidth="xs"
        onClose={() => {
          setOpenLogin(!openLogin);
        }}
        BackdropProps={{
          style: {
            background:
              'linear-gradient(74.49deg, rgba(146, 135, 89, 0.5) 46.08%, rgba(248, 248, 248, 0.5) 92.95%)',
          },
        }}
      >
        <LoginForm />
      </Dialog>
      <Dialog
        classes={{ paper: classes.registerDialog }}
        open={openRegister}
        TransitionComponent={Transition}
        onClose={() => {
          setOpenRegister(!openRegister);
        }}
        BackdropProps={{
          style: {
            background:
              'linear-gradient(74.49deg, rgba(146, 135, 89, 0.5) 46.08%, rgba(248, 248, 248, 0.5) 92.95%)',
          },
        }}
      >
        <RegisterForm />
      </Dialog>
    </div>
  );
  const secondHeader = (
    <AppBar
      className={classes.root}
      position="static"
      style={{ backgroundColor: '#F8F8F8' }}
    >
      <Toolbar>
        <Button
          className={classes.authButton}
          onClick={() => {
            history.push('/latest');
          }}
        >
          SKELBIMAI
        </Button>
        <Button
          className={classes.authButton}
          onClick={() => {
            history.push('/cv');
          }}
        >
          MANO CV
        </Button>
        <Button
          className={classes.authButton}
          onClick={() => {
            history.push('/profile');
          }}
        >
          PROFILIS
        </Button>
        <Button
          className={classes.authButton}
          onClick={() => {
            history.push('/plan');
          }}
        >
          PLANAI
        </Button>
      </Toolbar>
    </AppBar>
  );
  return <div>{isAuth ? secondHeader : mainHeader}</div>;
}

Header.propTypes = {};

export default Header;
