/**
 *
 * HowWorks
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Filter1OutlinedIcon from '@material-ui/icons/Filter1Outlined';
import Filter2OutlinedIcon from '@material-ui/icons/Filter2Outlined';
import Filter3OutlinedIcon from '@material-ui/icons/Filter3Outlined';
import Filter4OutlinedIcon from '@material-ui/icons/Filter4Outlined';
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import background from 'images/bg-for-contact.jpg';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    paddingTop: 120,
    paddingBottom: 90,
    background: `url(${background}) no-repeat`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
  },
  container: {
    position: 'relative',
    zIndex: 2,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    background:
      'linear-gradient(74.49deg, rgba(146, 135, 89, 0.5) 46.08%, rgba(248, 248, 248, 0.5) 92.95%)',
    zIndex: 1,
  },
  title: {
    marginBottom: 45,
  },
  subtitle: {
    marginBottom: 50,
  },
  choseContainer: {
    paddingLeft: 45,
  },
  contained: {
    color: '#ffffff',
    backgroundColor: '#ECC30B',
  },
  icon: {
    marginRight: 30,
    fill: '#E5E5E5',
  },
  contact: {
    display: 'flex',
    marginBottom: 60,
    justifyContent: 'flex-end',
  },
  contactItem: {
    display: 'flex',
    alignItems: 'center',
  },
}));

function HowWorks() {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.root}>
        {/* prettier-ignore */}
        <div className={classes.background}></div>
        <Container fixed className={classes.container}>
          <Typography
            variant="h3"
            component="h1"
            color="textSecondary"
            className={classes.title}
          >
            Kaip tai veikia ?
          </Typography>
          <div className={classes.choseContainer}>
            <div style={{ marginBottom: 40 }}>
              <Filter1OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Prisiregistruoji platformoje
              </Typography>
            </div>
            <div style={{ marginBottom: 40 }}>
              <Filter2OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Kanditatuoji į patinkančias darbo vietas
              </Typography>
            </div>
            <div style={{ marginBottom: 40 }}>
              <Filter3OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Gavęs patvirtinimą, pasirašai darbo sutartį mobiliu parašu ir
                vyksti dirbti nurodytu laiku.
              </Typography>
            </div>
            <div style={{ marginBottom: 100 }}>
              <Filter4OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Gauni atlygį į savo sąskaitą
              </Typography>
            </div>
            <div className={classes.contact}>
              <div style={{ marginRight: 60 }} className={classes.contactItem}>
                <PhoneIcon style={{ color: '#fff' }} />
                <Typography
                  variant="h5"
                  color="textSecondary"
                  component="span"
                  className={classes.info}
                >
                  +370 601 30 121
                </Typography>
              </div>
              <div className={classes.contactItem}>
                <MailIcon style={{ color: '#fff' }} />
                <Typography
                  variant="h5"
                  color="textSecondary"
                  component="span"
                  className={classes.info}
                >
                  info@pastas.lt
                </Typography>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}

HowWorks.propTypes = {};

export default HowWorks;
