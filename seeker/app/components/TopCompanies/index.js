/**
 *
 * TopCompanies
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles(() => ({
  container: {
    position: 'relative',
    zIndex: 5,
    marginTop: 120,
    marginBottom: -55,
  },
  title: {
    marginBottom: 50,
    fontWeight: 'bold',
  },
  paper: {
    padding: '55px 15px 18px 15px',
  },
  imgContainer: {
    textAlign: 'center',
  },
  img: {
    width: 160,
    marginBottom: 66,
  },
  description: {
    marginBottom: 20,
  },
  link: {
    color: '#ECC30B',
  },
}));
const data = [
  {
    img: 'http://www.statybuvizija.lt/images/logo.png',
    title: 'UAB Arto Statyba',
    description:
      'Jei nusprendėte statyti, atnaujinti esamą pastatą ar reikalinga konsultacija, kreipkitės į mus. Aptarsime jūsų poreikius, padarysime projektą ...',
    moreLink: '',
  },
  {
    img: 'http://www.statybuvizija.lt/images/logo.png',
    title: 'UAB Arto Statyba',
    description:
      'Jei nusprendėte statyti, atnaujinti esamą pastatą ar reikalinga konsultacija, kreipkitės į mus. Aptarsime jūsų poreikius, padarysime projektą ...',
    moreLink: '',
  },
  {
    img: 'http://www.statybuvizija.lt/images/logo.png',
    title: 'UAB Arto Statyba',
    description:
      'Jei nusprendėte statyti, atnaujinti esamą pastatą ar reikalinga konsultacija, kreipkitės į mus. Aptarsime jūsų poreikius, padarysime projektą ...',
    moreLink: '',
  },
];
function TopCompanies() {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      <Typography variant="h4" className={classes.title}>
        Top darbdaviai
      </Typography>
      <Grid container spacing={6}>
        {data.map(item => (
          <Grid item md={4}>
            <Paper className={classes.paper}>
              <div className={classes.imgContainer}>
                <img src={item.img} className={classes.img} />
              </div>
              <Typography variant="h5">{item.title}</Typography>
              <Typography variant="body1" className={classes.description}>
                {item.description}
              </Typography>
              <Link href="#" className={classes.link}>
                SKAITYKITE DAUGIAU
              </Link>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

TopCompanies.propTypes = {};

export default TopCompanies;
