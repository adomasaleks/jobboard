/*
 * TopCompanies Messages
 *
 * This contains all the text for the TopCompanies component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.TopCompanies';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the TopCompanies component!',
  },
});
