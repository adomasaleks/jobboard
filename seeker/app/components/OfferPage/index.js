/**
 *
 * OfferPage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import ServiceOfferForm from 'containers/ServiceOfferForm';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 90,
  },
}));

function OfferPage() {
  const classes = useStyles();
  return (
    <>
      <Container className={classes.container}>
        <Grid container justify="center">
          <Grid item md={9}>
            <ServiceOfferForm />
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

OfferPage.propTypes = {};

export default OfferPage;
