/*
 * JobsTable Messages
 *
 * This contains all the text for the JobsTable component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.JobsTable';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the JobsTable component!',
  },
});
