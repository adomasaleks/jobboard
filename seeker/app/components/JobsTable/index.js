/**
 *
 * JobsTable
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';

//Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

//Components
import JobOfferDialog from 'components/JobOfferDialog';

const useStyles = makeStyles(theme => ({
  row: {
    cursor: 'pointer',
  },
  image: {
    width: 170,
  },
}));

function JobsTable({ data }) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [dialogData, setDialogData] = React.useState(null);

  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const openOfferDialog = data => {
    setDialogData(data);
  };
  const closeOfferDialog = () => {
    setDialogData(null);
  };
  return (
    <div>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableBody>
            {data.map(row => (
              <TableRow
                key={row.name}
                className={classes.row}
                onClick={() => {
                  openOfferDialog(row);
                }}
              >
                <TableCell component="th" scope="row">
                  <img
                    className={classes.image}
                    src="http://www.statybuvizija.lt/images/logo.png"
                  />
                </TableCell>
                <TableCell align="center">{row.name}</TableCell>
                <TableCell align="center">{row.companyName}</TableCell>
                <TableCell align="center">
                  Adresas, <br />
                  {row.city}
                </TableCell>
                <TableCell align="center">{row.salaryFrom} Eur</TableCell>
                <TableCell align="center">{row.createdAt}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <JobOfferDialog data={dialogData} onClose={closeOfferDialog} />
    </div>
  );
}

JobsTable.propTypes = {};

export default JobsTable;
