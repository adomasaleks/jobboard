/*
 * HomeHeroSection Messages
 *
 * This contains all the text for the HomeHeroSection component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.HomeHeroSection';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the HomeHeroSection component!',
  },
});
