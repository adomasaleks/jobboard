/*
 * RequirementServiceForm Messages
 *
 * This contains all the text for the RequirementServiceForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RequirementServiceForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RequirementServiceForm component!',
  },
});
