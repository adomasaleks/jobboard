/**
 *
 * RequirementServiceForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';

import messages from './messages';

const useStyles = makeStyles(() => ({
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  header: {
    marginTop: 40,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
}));

function RequirementServiceForm({ values, actions }) {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={6}>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="title"
            label="Paslaugos pavadinimas"
            variant="outlined"
            value={values.title}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="offerPosition"
            label="Paslaugos sritis"
            variant="outlined"
            value={values.offerPosition}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={12}>
          <TextField
            fullWidth
            id="workDescription"
            label="Paslaugos aprašymas"
            multiline
            rows={7}
            variant="outlined"
            value={values.workDescription}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.header}>
            Paslaugos kainų rėžiai
          </Typography>
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="salaryFrom"
            label="Nuo"
            variant="outlined"
            value={values.salaryFrom}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  className={classes.textField}
                  position="end"
                  disableTypography
                >
                  €
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="salaryTo"
            label="Iki"
            variant="outlined"
            value={values.salaryTo}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  className={classes.textField}
                  position="end"
                  disableTypography
                >
                  €
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.header}>
            Atliekamos paslaugos vieta
          </Typography>
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="companyName"
            label="Įmonė/ Paslaugos vykdytojas"
            variant="outlined"
            value={values.companyName}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="showCompanyName"
            label="Skelbime rodomas įmonės/ vykdytojo pavadinimas"
            variant="outlined"
            helperText="PATARIMAS: Įmonės/ vykdytojo pavadinimas, kuris bus rodomas viešai"
            value={values.showCompanyName}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="city"
            label="Miestas"
            variant="outlined"
            value={values.city}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="address"
            label="Adresas"
            variant="outlined"
            value={values.address}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}

RequirementServiceForm.propTypes = {
  values: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
};

export default RequirementServiceForm;
