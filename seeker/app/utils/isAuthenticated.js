export default function isAuthenticated() {
  const expTime = localStorage.getItem('expTime');
  return new Date().getTime() / 1000 <= expTime && expTime !== null;
}
