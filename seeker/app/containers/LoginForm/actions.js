/*
 *
 * LoginForm actions
 *
 */

import { UPDATE_FIELD, POST_LOGIN, POST_LOGIN_SUCCESS } from './constants';

export function updateFieldAction(fieldId, value) {
  return {
    type: UPDATE_FIELD,
    fieldId,
    value,
  };
}

export function postLoginAction() {
  return {
    type: POST_LOGIN,
  };
}

export function postLoginSuccess() {
  return {
    type: POST_LOGIN_SUCCESS,
  };
}
