/*
 *
 * LoginForm constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginForm/DEFAULT_ACTION';

export const UPDATE_FIELD = 'app/LoginForm/UPDATE_FIELD';

export const POST_LOGIN = 'app/LoginForm/POST_LOGIN';

export const POST_LOGIN_SUCCESS = 'app/LoginForm/POST_LOGIN_SUCCESS';

export const POST_LOGIN_ERROR = 'app/LoginForm/POST_LOGIN_ERROR';
