import { call, put, select, takeLatest } from 'redux-saga/effects';
import { postLoginCall } from 'containers/API/user';
import history from 'utils/history';
import { authSuccess } from 'containers/Auth/actions';
import { POST_LOGIN } from './constants';
import makeSelectLoginForm from './selectors';
import { postLoginSuccess } from './actions';

function* postLoginSaga() {
  const state = yield select(makeSelectLoginForm());

  try {
    const res = yield call(postLoginCall, state);
    yield put(postLoginSuccess());
    localStorage.setItem('token', res.data.accessToken);
    localStorage.setItem(
      'expTime',
      new Date().getTime() / 1000 + res.data.expiresIn,
    );
    yield put(
      authSuccess(
        res.data.accessToken,
        new Date().getTime() / 1000 + res.data.expiresIn,
      ),
    );
    yield put(history.push('/latest'));
  } catch (e) {
    console.log(e);
  }
}
// Individual exports for testing
export default function* loginFormSaga() {
  yield takeLatest(POST_LOGIN, postLoginSaga);
}
