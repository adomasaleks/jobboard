/*
 *
 * LoginForm reducer
 *
 */
import produce from 'immer';
import { UPDATE_FIELD, POST_LOGIN_SUCCESS } from './constants';

export const initialState = {
  mail: '',
  password: '',
};

/* eslint-disable default-case, no-param-reassign */
const loginFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_FIELD:
        draft[action.fieldId] = action.value;
        break;
      case POST_LOGIN_SUCCESS:
        return { ...initialState };
    }
  });

export default loginFormReducer;
