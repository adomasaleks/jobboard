/**
 *
 * LoginForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//material-ui
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLoginForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import { updateFieldAction, postLoginAction } from './actions';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  dialog: {
    padding: theme.spacing(6, 4, 5, 4),
  },
  container: {
    marginTop: theme.spacing(3),
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  button: {
    backgroundColor: '#4D7298',
    color: '#ffffff',
  },
  text: {
    marginTop: theme.spacing(2),
  },
}));

export function LoginForm({ updateFieldAction, loginForm, postLoginAction }) {
  useInjectReducer({ key: 'loginForm', reducer });
  useInjectSaga({ key: 'loginForm', saga });

  const classes = useStyles();

  return (
    <div className={classes.dialog}>
      <Typography variant="h3" align="center">
        Prisijungti
      </Typography>
      <Grid
        container
        spacing={6}
        className={classes.container}
        justify="center"
      >
        <Grid item md={12}>
          <TextField
            id="mail"
            fullWidth
            label="El. Paštas"
            value={loginForm.mail}
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            variant="outlined"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={12}>
          <TextField
            id="password"
            fullWidth
            type="password"
            label="Slaptažodis"
            value={loginForm.password}
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            variant="outlined"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid container item md={12} alignItems="center" justify="space-evenly">
          <Button
            variant="contained"
            className={classes.button}
            onClick={postLoginAction}
          >
            Prisijungti
          </Button>
          <Typography className={classes.text}>
            Užmiršote slaptažodį? <Link>Išsiųsti priminimą</Link>
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}

LoginForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
  loginForm: makeSelectLoginForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ updateFieldAction, postLoginAction }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LoginForm);
