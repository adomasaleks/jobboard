/*
 *
 * CvForm constants
 *
 */

export const UPDATE_TEXT_FIELD = 'app/CvForm/UPDATE_TEXT_FIELD';

export const ADD_ARRAY_ELEMENT = 'app/CvForm/ADD_ARRAY_ELEMENT';
export const UPDATE_ARRAY_ELEMENT = 'app/CvForm/UPDATE_ARRAY_ELEMENT';
export const DELETE_ARRAY_ELEMENT = 'app/CvForm/DELETE_ARRAY_ELEMENT';

export const POST_CV = 'app/CvForm/POST_CV';
export const POST_CV_SUCCESS = 'app/CvForm/POST_CV_SUCCESS';
export const POST_CV_ERROR = 'app/CvForm/POST_CV_ERROR';

export const GET_CV = 'app/CvForm/GET_CV';
export const GET_CV_SUCCESS = 'app/CvForm/GET_CV_SUCCESS';
export const GET_CV_ERROR = 'app/CvForm/GET_CV_ERROR';

export const PUT_CV = 'app/CvForm/PUT_CV';
export const PUT_CV_SUCCESS = 'app/CvForm/PUT_CV_SUCCESS';
export const PUT_CV_ERROR = 'app/CvForm/PUT_CV_ERROR';
