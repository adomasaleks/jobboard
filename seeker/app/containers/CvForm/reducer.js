/*
 *
 * CvForm reducer
 *
 */
import produce from 'immer';
import {
  UPDATE_TEXT_FIELD,
  UPDATE_ARRAY_ELEMENT,
  ADD_ARRAY_ELEMENT,
  DELETE_ARRAY_ELEMENT,
  POST_CV_SUCCESS,
  GET_CV_SUCCESS,
} from './constants';
export const initialState = {
  id: '',
  name: '',
  lastName: '',
  education: [
    {
      school: '',
      dateFrom: new Date(),
      dateTo: new Date(),
    },
  ],
  courses: [
    {
      courseName: '',
      dateFrom: new Date(),
      dateTo: new Date(),
    },
  ],
  additionalInformation: '',
};

/* eslint-disable default-case, no-param-reassign */
const cvFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_TEXT_FIELD:
        draft[action.fieldId] = action.data;
        break;
      case ADD_ARRAY_ELEMENT:
        draft[action.fieldId].push({
          [action.fieldName]: '',
          dateFrom: new Date(),
          dateTo: new Date(),
        });
        break;
      case UPDATE_ARRAY_ELEMENT:
        draft[action.name][action.id][action.fieldId] = action.data;
        break;
      case DELETE_ARRAY_ELEMENT:
        draft[action.name].splice(action.id, 1);
        break;
      case POST_CV_SUCCESS:
        return { ...initialState };
      case GET_CV_SUCCESS:
        draft.id = action.data.id;
        draft.name = action.data.name;
        draft.lastName = action.data.lastName;
        draft.education = action.data.education || [
          {
            school: '',
            dateFrom: new Date(),
            dateTo: new Date(),
          },
        ];
        draft.courses = action.data.courses || [
          {
            school: '',
            dateFrom: new Date(),
            dateTo: new Date(),
          },
        ];
        draft.additionalInformation = action.data.additionalInformation || '';
        break;
    }
  });

export default cvFormReducer;
