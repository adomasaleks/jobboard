/**
 *
 * CvForm
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//Material-ui
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectCvForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  updateTextField,
  addArray,
  updateArray,
  deleteArray,
  postCV,
  getCVAction,
  putCVAction,
} from './actions';

const useStyles = makeStyles(() => ({
  container: {
    paddingTop: 90,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  grid: {
    marginTop: 60,
  },
  gridArray: {
    marginBottom: 24,
  },
  subTitle: {
    paddingBottom: '0px!important',
  },
  subTitle2: {
    paddingBottom: 24,
  },
  flexEnd: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  buttonAdd: {
    width: 48,
    height: 48,
    backgroundColor: '#56CCF2',
    color: '#ffffff',
  },
  buttonDelete: {
    width: 48,
    height: 48,
    backgroundColor: '#E10050',
    color: '#ffffff',
  },
  button: {
    marginTop: 60,
    marginBottom: 50,
    backgroundColor: '#56CCF2',
    color: '#fff',
  },
}));

export function CvForm({
  cvForm,
  updateTextField,
  addArray,
  updateArray,
  deleteArray,
  postCV,
  getCVAction,
  putCVAction,
}) {
  useInjectReducer({ key: 'cvForm', reducer });
  useInjectSaga({ key: 'cvForm', saga });
  const [selectedDate, handleDateChange] = useState(new Date());

  const classes = useStyles();

  useEffect(() => {
    getCVAction();
  }, []);

  const addArrayElement = name => {
    if (name === 'education') {
      addArray(name, 'school');
    } else {
      addArray(name, 'courseName');
    }
  };

  const updateArrayElement = (name, id, fieldId, data) => {
    if (fieldId === 'dateFrom' || fieldId === 'dateTo') {
      updateArray(name, id, fieldId, new Date(data));
    } else {
      updateArray(name, id, fieldId, data);
    }
  };

  const deleteArrayElement = (name, id) => {
    deleteArray(name, id);
  };

  return (
    <Container className={classes.container}>
      <Grid container>
        <Grid item md={12}>
          <Typography variant="h3">Naujas gyvenimo aprašymas</Typography>
        </Grid>
      </Grid>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container justify="center">
          <Grid item md={9}>
            <Grid container spacing={6} className={classes.grid}>
              <Grid item md={12} className={classes.subTitle}>
                <Typography variant="h5">Apie mane</Typography>
              </Grid>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="name"
                  label="Vardas"
                  variant="outlined"
                  value={cvForm.name}
                  onChange={e => {
                    updateTextField(e.target.id, e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="lastName"
                  label="Pavardė"
                  variant="outlined"
                  value={cvForm.lastName}
                  onChange={e => {
                    updateTextField(e.target.id, e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container className={classes.grid}>
              <Grid item md={12} className={classes.subTitle2}>
                <Typography variant="h5">Išsilavinimas</Typography>
              </Grid>
              {cvForm.education.map((currentValue, id, item) => (
                <Grid
                  container
                  key={`${id}-courses`}
                  spacing={6}
                  className={classes.gridArray}
                >
                  <Grid item md={6}>
                    <TextField
                      fullWidth
                      id={`school-${id}`}
                      label="Mokymo įstaigos pavadinimas"
                      variant="outlined"
                      value={item[id].school}
                      onChange={e => {
                        updateArrayElement(
                          'education',
                          id,
                          'school',
                          e.target.value,
                        );
                      }}
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                    />
                  </Grid>
                  <Grid item md={6} className={classes.flexEnd}>
                    {id < cvForm.education.length - 1 ? (
                      <IconButton
                        classes={{ root: classes.buttonDelete }}
                        onClick={() => {
                          deleteArrayElement('education', id);
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    ) : (
                      <IconButton
                        classes={{ root: classes.buttonAdd }}
                        onClick={() => {
                          addArrayElement('education');
                        }}
                      >
                        <AddIcon />
                      </IconButton>
                    )}
                  </Grid>
                  <Grid item md={6}>
                    <DatePicker
                      fullWidth
                      disableFuture
                      openTo="year"
                      format="dd/MM/yyyy"
                      label="Data nuo"
                      value={item[id].dateFrom}
                      views={['year', 'month', 'date']}
                      inputVariant="outlined"
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                      onChange={e => {
                        updateArrayElement('education', id, 'dateFrom', e);
                      }}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <DatePicker
                      fullWidth
                      disableFuture
                      openTo="year"
                      format="dd/MM/yyyy"
                      label="Data iki"
                      value={item[id].dateTo}
                      views={['year', 'month', 'date']}
                      inputVariant="outlined"
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                      onChange={e => {
                        updateArrayElement('education', id, 'dateTo', e);
                      }}
                    />
                  </Grid>
                </Grid>
              ))}
            </Grid>
            <Grid container className={classes.grid}>
              <Grid item md={12} className={classes.subTitle2}>
                <Typography variant="h5">Kursai, sertifikatai</Typography>
              </Grid>
              {cvForm.courses.map((currentValue, id, item) => (
                <Grid
                  container
                  key={`${id}-courses`}
                  spacing={6}
                  className={classes.gridArray}
                >
                  <Grid item md={6}>
                    <TextField
                      fullWidth
                      id={`course-${id}`}
                      label="Išdavusios įstaigos pavadinimas"
                      variant="outlined"
                      value={item[id].courseName}
                      onChange={e => {
                        updateArrayElement(
                          'courses',
                          id,
                          'courseName',
                          e.target.value,
                        );
                      }}
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                    />
                  </Grid>
                  <Grid item md={6} className={classes.flexEnd}>
                    {id < cvForm.courses.length - 1 ? (
                      <IconButton
                        classes={{ root: classes.buttonDelete }}
                        onClick={() => {
                          deleteArrayElement('courses', id);
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    ) : (
                      <IconButton
                        classes={{ root: classes.buttonAdd }}
                        onClick={() => {
                          addArrayElement('courses');
                        }}
                      >
                        <AddIcon />
                      </IconButton>
                    )}
                  </Grid>
                  <Grid item md={6}>
                    <DatePicker
                      fullWidth
                      disableFuture
                      openTo="year"
                      format="dd/MM/yyyy"
                      label="Data nuo"
                      value={item[id].dateFrom}
                      views={['year', 'month', 'date']}
                      inputVariant="outlined"
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                      onChange={e => {
                        updateArrayElement('courses', id, 'dateFrom', e);
                      }}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <DatePicker
                      fullWidth
                      disableFuture
                      openTo="year"
                      format="dd/MM/yyyy"
                      label="Data iki"
                      value={item[id].dateTo}
                      views={['year', 'month', 'date']}
                      inputVariant="outlined"
                      InputLabelProps={{
                        className: classes.textField,
                      }}
                      onChange={e => {
                        updateArrayElement('courses', id, 'dateTo', e);
                      }}
                    />
                  </Grid>
                </Grid>
              ))}
            </Grid>
            <Grid container spacing={6} className={classes.grid}>
              <Grid item md={12} className={classes.subTitle}>
                <Typography variant="h5">Papildoma informacija</Typography>
              </Grid>
              <Grid item md={12}>
                <TextField
                  fullWidth
                  multiline
                  rows={5}
                  id="additionalInformation"
                  label="Papildoma informacija"
                  variant="outlined"
                  value={cvForm.additionalInformation}
                  onChange={e => {
                    updateTextField(e.target.id, e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container>
              <Grid item md={12} className={classes.flexEnd}>
                <Button
                  variant="contained"
                  className={classes.button}
                  onClick={!!cvForm.id ? putCVAction : postCV}
                >
                  Išsaugoti
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
    </Container>
  );
}

CvForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
  cvForm: makeSelectCvForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateTextField,
      addArray,
      updateArray,
      deleteArray,
      postCV,
      getCVAction,
      putCVAction,
    },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CvForm);
