/*
 *
 * CvForm actions
 *
 */

import {
  UPDATE_TEXT_FIELD,
  UPDATE_ARRAY_ELEMENT,
  ADD_ARRAY_ELEMENT,
  DELETE_ARRAY_ELEMENT,
  POST_CV,
  POST_CV_SUCCESS,
  POST_CV_ERROR,
  GET_CV,
  GET_CV_SUCCESS,
  PUT_CV,
  PUT_CV_SUCCESS,
} from './constants';

export function updateTextField(fieldId, data) {
  return {
    type: UPDATE_TEXT_FIELD,
    fieldId,
    data,
  };
}
export function updateArray(name, id, fieldId, data) {
  return {
    type: UPDATE_ARRAY_ELEMENT,
    name,
    id,
    fieldId,
    data,
  };
}
export function addArray(fieldId, fieldName) {
  return {
    type: ADD_ARRAY_ELEMENT,
    fieldId,
    fieldName,
  };
}
export function deleteArray(name, id) {
  return {
    type: DELETE_ARRAY_ELEMENT,
    name,
    id,
  };
}

export function postCV() {
  return {
    type: POST_CV,
  };
}

export function postCVSuccess() {
  return {
    type: POST_CV_SUCCESS,
  };
}

export function postCVError() {
  return {
    type: POST_CV_ERROR,
  };
}

export function getCVAction() {
  return {
    type: GET_CV,
  };
}

export function getCVSuccessAction(data) {
  return {
    type: GET_CV_SUCCESS,
    data,
  };
}

export function putCVAction() {
  return {
    type: PUT_CV,
  };
}

export function putCVSuccessAction() {
  return {
    type: PUT_CV_SUCCESS,
  };
}
