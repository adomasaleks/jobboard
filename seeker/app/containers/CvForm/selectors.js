import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the cvForm state domain
 */

const selectCvFormDomain = state => state.cvForm || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CvForm
 */

const makeSelectCvForm = () =>
  createSelector(
    selectCvFormDomain,
    substate => substate,
  );

export default makeSelectCvForm;
export { selectCvFormDomain };
