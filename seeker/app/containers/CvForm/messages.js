/*
 * CvForm Messages
 *
 * This contains all the text for the CvForm container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.CvForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CvForm container!',
  },
});
