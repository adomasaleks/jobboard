import { call, put, select, takeLatest } from 'redux-saga/effects';
import { postCV, getCVCall, putCVCall } from 'containers/API/cv';
import { POST_CV, GET_CV, PUT_CV } from './constants';
import makeSelectCvForm from './selectors';
import {
  postCVSuccess,
  getCVSuccessAction,
  putCVSuccessAction,
} from './actions';
import history from 'utils/history';
function* submitCV() {
  const state = yield select(makeSelectCvForm());
  try {
    const res = yield call(postCV, state);
    yield put(postCVSuccess());
    history.push('/latest');
  } catch (e) {
    console.log(e);
  }
  console.log(state);
}

function* getCVSaga() {
  try {
    const res = yield call(getCVCall);
    yield put(getCVSuccessAction(res.data));
  } catch (e) {
    console.log(e);
  }
}

function* putCVSaga() {
  const state = yield select(makeSelectCvForm());
  try {
    yield call(putCVCall, state);
    yield put(putCVSuccessAction());
    history.push('/latest');
  } catch (e) {
    console.log(e);
  }
  console.log(state);
}

// Individual exports for testing
export default function* cvFormSaga() {
  yield takeLatest(GET_CV, getCVSaga);
  yield takeLatest(POST_CV, submitCV);
  yield takeLatest(PUT_CV, putCVSaga);
}
