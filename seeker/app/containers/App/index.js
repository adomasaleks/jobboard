/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Auth from 'containers/Auth';
import HeaderContainer from 'containers/HeaderContainer';
import HomePage from 'containers/HomePage';
import LatestJobsPage from 'containers/LatestJobsPage';
import CvForm from 'containers/CvForm';
import ProfilePage from 'containers/ProfilePage';
import OfferPage from 'components/OfferPage';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Footer from 'components/Footer';

export default function App() {
  return (
    <div>
      <Auth />
      <HeaderContainer />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/latest" component={LatestJobsPage} />
        <Route exact path="/cv" component={CvForm} />
        <Route exact path="/profile" component={ProfilePage} />
        <Route exact path="/offer" component={OfferPage} />
        <Route exact path="/offer/:id" component={OfferPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <Footer />
    </div>
  );
}
