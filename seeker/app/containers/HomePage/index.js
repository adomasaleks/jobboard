/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import HomeHeroSection from 'components/HomeHeroSection';
import TopCompanies from 'components/TopCompanies';
import HowWorks from 'components/HowWorks';

export default function HomePage() {
  return (
    <div>
      <HomeHeroSection />
      <TopCompanies />
      <HowWorks />
    </div>
  );
}
