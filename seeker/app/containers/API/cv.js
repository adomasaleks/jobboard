import axios from 'axios';
import { authHeader } from 'utils/authHeader';
export function postCV(data) {
  return axios.post('http://localhost:3001/api/create-cv', data, {
    headers: authHeader(),
  });
}

export function getCVCall() {
  return axios.get('http://localhost:3001/api/get-cv', {
    headers: authHeader(),
  });
}

export function putCVCall(data) {
  return axios.put(`http://localhost:3001/api/update-cv/${data.id}`, data, {
    headers: authHeader(),
  });
}
