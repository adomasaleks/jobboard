import axios from 'axios';
import { authHeader } from 'utils/authHeader';
export function postLoginCall(state) {
  return axios.post('http://localhost:3001/api/signin', {
    email: state.mail,
    password: state.password,
  });
}

export function postRegisterCall(state) {
  return axios.post('http://localhost:3001/api/signup', {
    email: state.mail,
    password: state.password,
    phoneNumber: state.mobile,
  });
}

export function putPasswordCall(state) {
  return axios.put(
    'http://localhost:3001/api/update-seeker-password',
    {
      password: state.oldPassword,
    },
    {
      headers: authHeader(),
    },
  );
}
