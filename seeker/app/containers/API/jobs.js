import axios from 'axios';

export function fetchLatestJobs() {
  return axios.get('http://localhost:3001/api/get-all-offers');
}
