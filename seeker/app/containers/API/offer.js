import axios from 'axios';
import { authHeader } from 'utils/authHeader';

export function fetchOffer(id) {
  return axios.get(`http://localhost:3001/api/get-recruiter-offers/${id}`, {
    headers: authHeader(),
  });
}

export function fetchPostOffer(state) {
  return axios.post('http://localhost:3001/api/create-offer', state, {
    headers: authHeader(),
  });
}

export function updateOffer(id, state) {
  return axios.put(`http://localhost:3001/api/update-offer/${id}`, state, {
    headers: authHeader(),
  });
}
