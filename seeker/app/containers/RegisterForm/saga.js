import { call, put, select, takeLatest } from 'redux-saga/effects';
import { postRegisterCall } from 'containers/API/user';
import { authSuccess } from 'containers/Auth/actions';
import history from 'utils/history';
import { POST_REGISTER } from './constants';
import { postRegisterAction } from './actions';

import makeSelectRegisterForm from './selectors';

function* postRegisterSaga() {
  const state = yield select(makeSelectRegisterForm());
  try {
    const res = yield call(postRegisterCall, state);
    yield put(postRegisterAction());
    localStorage.setItem('token', res.data.accessToken);
    localStorage.setItem(
      'expTime',
      new Date().getTime() / 1000 + res.data.expiresIn,
    );
    yield put(
      authSuccess(
        res.data.accessToken,
        new Date().getTime() / 1000 + res.data.expiresIn,
      ),
    );
    yield put(history.push('/latest'));
  } catch (e) {
    console.log(e);
  }
  console.log(state);
}

// Individual exports for testing
export default function* registerFormSaga() {
  yield takeLatest(POST_REGISTER, postRegisterSaga);
}
