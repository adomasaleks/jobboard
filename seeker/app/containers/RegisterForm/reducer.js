/*
 *
 * RegisterForm reducer
 *
 */
import produce from 'immer';
import { UPDATE_FIELD, POST_REGISTER_SUCCESS } from './constants';

export const initialState = {
  mail: '',
  password: '',
  name: '',
  mobile: '',
};

/* eslint-disable default-case, no-param-reassign */
const registerFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_FIELD:
        draft[action.fieldId] = action.value;
        break;
      case POST_REGISTER_SUCCESS:
        return { ...initialState };
    }
  });

export default registerFormReducer;
