/**
 *
 * RegisterForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//Material-ui

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectRegisterForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import { updateFieldAction, postRegisterAction } from './actions';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  dialog: {
    padding: theme.spacing(6, 4, 5, 4),
  },
  title: {
    marginBottom: 50,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  grid: {
    marginBottom: 30,
  },
  button: {
    backgroundColor: '#4D7298',
    color: '#ffffff',
    marginRight: 25,
  },
}));

export function RegisterForm({
  registerForm,
  updateFieldAction,
  postRegisterAction,
}) {
  useInjectReducer({ key: 'registerForm', reducer });
  useInjectSaga({ key: 'registerForm', saga });
  const classes = useStyles();
  return (
    <div className={classes.dialog}>
      <Typography variant="h4" align="center" className={classes.title}>
        Sukurti naują vartotoją
      </Typography>
      <Grid container spacing={5}>
        <Grid item md={6}>
          <TextField
            id="mail"
            fullWidth
            label="El. Paštas"
            variant="outlined"
            value={registerForm.mail}
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            id="password"
            fullWidth
            type="password"
            label="Slaptažodis"
            value={registerForm.password}
            variant="outlined"
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={5} className={classes.grid}>
        <Grid item md={6}>
          <TextField
            id="name"
            fullWidth
            label="Jūsų vardas"
            value={registerForm.name}
            variant="outlined"
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            id="mobile"
            fullWidth
            label="Telefono numeris"
            value={registerForm.mobile}
            variant="outlined"
            onChange={e => {
              updateFieldAction(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container alignItems="center" className={classes.grid}>
        <Button
          variant="contained"
          className={classes.button}
          onClick={postRegisterAction}
        >
          Registruotis
        </Button>
        <Typography>
          Turite paskyrą? <Link>Prisijunkite</Link>
        </Typography>
      </Grid>
      <Grid container alignItems="center">
        <Checkbox
          icon={
            <CheckBoxOutlineBlankIcon
              style={{ color: 'rgba(0, 0, 0, 0.54)' }}
            />
          }
        />
        <Typography>
          sutinku su puslapio <Link>sąlygomis ir taisyklėmis</Link>
        </Typography>
      </Grid>
    </div>
  );
}

RegisterForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
  registerForm: makeSelectRegisterForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { updateFieldAction, postRegisterAction },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(RegisterForm);
