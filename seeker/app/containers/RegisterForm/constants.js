/*
 *
 * RegisterForm constants
 *
 */

export const UPDATE_FIELD = 'app/RegisterForm/UPDATE_FIELD';

export const POST_REGISTER = 'app/RegisterForm/POST_REGISTER';
export const POST_REGISTER_SUCCESS = 'app/RegisterForm/POST_REGISTER_SUCCESS';
export const POST_REGISTER_ERROR = 'app/RegisterForm/POST_REGISTER_ERROR';
