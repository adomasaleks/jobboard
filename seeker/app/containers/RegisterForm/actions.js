/*
 *
 * RegisterForm actions
 *
 */

import { UPDATE_FIELD, POST_REGISTER } from './constants';

export function updateFieldAction(fieldId, value) {
  return {
    type: UPDATE_FIELD,
    fieldId,
    value,
  };
}

export function postRegisterAction() {
  return {
    type: POST_REGISTER,
  };
}
