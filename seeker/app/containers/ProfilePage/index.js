/**
 *
 * ProfilePage
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//Material-ui
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectProfilePage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { updatePasswordAction } from './actions';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
  },
  subTitle: {
    marginTop: 60,
    marginBottom: 35,
  },
  passwordTitle: {
    marginTop: 65,
    marginBottom: 35,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  button: {
    marginTop: 35,
    textAlign: 'end',
  },
  saveButton: {
    backgroundColor: '#56CCF2',
    color: '#ffffff',
  },
}));

export function ProfilePage({ updatePasswordAction }) {
  useInjectReducer({ key: 'profilePage', reducer });
  useInjectSaga({ key: 'profilePage', saga });

  const [mobile, setMobile] = useState('');
  const [email, setEmail] = useState('');

  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');

  const classes = useStyles();

  const submitPassword = () => {
    updatePasswordAction({ oldPassword, newPassword });
  };

  return (
    <div>
      <Container className={classes.container}>
        <Grid container justify="center">
          <Grid item md={10}>
            <Grid container>
              <Grid item md={12}>
                <Typography variant="h3">Mano profilis</Typography>
              </Grid>
            </Grid>
            <Grid container className={classes.subTitle}>
              <Grid item md={12}>
                <Typography variant="h5">Asmeninė informacija</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="email"
                  label="Elektroninis paštas"
                  variant="outlined"
                  value={email}
                  onChange={e => {
                    setEmail(e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="phone"
                  label="Telefono numeris"
                  variant="outlined"
                  value={mobile}
                  onChange={e => {
                    setMobile(e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container>
              <Grid item md={12} className={classes.button}>
                <Button variant="contained" className={classes.saveButton}>
                  Saugoti
                </Button>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item md={12} className={classes.passwordTitle}>
                <Typography variant="h5">Pakeisti slaptažodį</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="currentPassword"
                  label="Dabartinis slaptažodis"
                  type="password"
                  variant="outlined"
                  value={oldPassword}
                  onChange={e => {
                    setOldPassword(e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="newPassword"
                  label="Naujas slaptažodis"
                  type="password"
                  variant="outlined"
                  value={newPassword}
                  onChange={e => {
                    setNewPassword(e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
              <Grid item md={6}>
                <TextField
                  fullWidth
                  id="confirmNewPassword"
                  label="Pakartokite naują slaptažodį"
                  type="password"
                  variant="outlined"
                  value={repeatPassword}
                  onChange={e => {
                    setRepeatPassword(e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container>
              <Grid item md={12} className={classes.button}>
                <Button
                  variant="contained"
                  className={classes.saveButton}
                  onClick={submitPassword}
                >
                  Saugoti
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

ProfilePage.propTypes = {};

const mapStateToProps = createStructuredSelector({
  profilePage: makeSelectProfilePage(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ updatePasswordAction }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProfilePage);
