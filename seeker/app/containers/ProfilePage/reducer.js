/*
 *
 * ProfilePage reducer
 *
 */
import produce from 'immer';
import { UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS } from './constants';

export const initialState = {
  oldPassword: '',
  newPassword: '',
};

/* eslint-disable default-case, no-param-reassign */
const profilePageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_PASSWORD:
        draft.oldPassword = action.values.oldPassword;
        draft.newPassword = action.values.newPassword;
        break;
      case UPDATE_PASSWORD_SUCCESS:
        return { ...initialState };
    }
  });

export default profilePageReducer;
