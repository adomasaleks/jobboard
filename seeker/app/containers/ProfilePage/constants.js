/*
 *
 * ProfilePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProfilePage/DEFAULT_ACTION';

export const UPDATE_PASSWORD = 'app/ProfilePage/UPDATE_PASSWORD';
export const UPDATE_PASSWORD_SUCCESS =
  'app/ProfilePage/UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_ERROR = 'app/ProfilePage/UPDATE_PASSWORD_ERROR';
