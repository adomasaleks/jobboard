import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import makeSelectProfilePage from './selectors';
import { UPDATE_PASSWORD } from './constants';
import { putPasswordCall } from 'containers/API/user';
function* putPasswordSaga() {
  const state = yield select(makeSelectProfilePage());
  console.log(state);
  try {
    const res = yield call(putPasswordCall, state);
    console.log(res);
  } catch (e) {
    console.log(e);
  }
}

// Individual exports for testing
export default function* profilePageSaga() {
  yield takeLatest(UPDATE_PASSWORD, putPasswordSaga);
}
