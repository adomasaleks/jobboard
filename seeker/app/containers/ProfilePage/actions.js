/*
 *
 * ProfilePage actions
 *
 */

import { UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS } from './constants';

export function updatePasswordAction(values) {
  return {
    type: UPDATE_PASSWORD,
    values,
  };
}

export function updatePasswordSuccessAction() {
  return {
    type: UPDATE_PASSWORD_SUCCESS,
  };
}
