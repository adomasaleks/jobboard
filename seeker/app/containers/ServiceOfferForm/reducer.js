/*
 *
 * ServiceOfferForm reducer
 *
 */
import produce from 'immer';
import {
  UPDATE_TEXT_FIELD,
  UPLOAD_FILE,
  DELETE_FILE,
  CREATE_SERVICE_OFFER_SUCCESS,
  GET_SERVICE_OFFER,
  GET_SERVICE_OFFER_SUCCESS,
} from './constants';

export const initialState = {
  id: '',
  title: '',
  offerPosition: '',
  workDescription: '',
  salaryFrom: '',
  salaryTo: '',
  companyName: '',
  showCompanyName: '',
  city: '',
  address: '',
  file: [],
};

/* eslint-disable default-case, no-param-reassign */
const serviceOfferFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_TEXT_FIELD:
        draft[action.fieldId] = action.data;
        break;
      case UPLOAD_FILE:
        draft.file = action.file;
        break;
      case DELETE_FILE:
        draft.file = [];
        break;
      case CREATE_SERVICE_OFFER_SUCCESS:
        return initialState;
      case GET_SERVICE_OFFER:
        draft.id = action.id;
        break;
      case GET_SERVICE_OFFER_SUCCESS:
        return { ...action.data };
    }
  });

export default serviceOfferFormReducer;
