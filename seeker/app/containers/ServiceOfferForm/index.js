/**
 *
 * ServiceOfferForm
 *
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//components
import DropZoneServiceForm from 'components/DropZoneServiceForm';
import RequirementServiceForm from 'components/RequirementServiceForm';

import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectServiceOfferForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  updateTextField,
  uploadFile,
  deleteFile,
  createOffer,
  updateOffer,
  fetchEditOffer,
} from './actions';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 90,
  },
  header: {
    marginBottom: 60,
  },
  root: {
    backgroundColor: 'transparent',
    transition: 'none',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(8),
    textAlign: 'end',
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  notActive: {
    color: 'rgba(0, 0, 0, 0.36)',
  },
  backButton: {
    marginRight: 15,
  },
}));

function getSteps() {
  return ['Paslaugos Aprašymas', 'Atvaizdavimas'];
}

export function ServiceOfferForm({
  serviceOfferForm,
  updateTextField,
  uploadFile,
  deleteFile,
  createOffer,
  updateOffer,
  fetchEditOffer,
}) {
  useInjectReducer({ key: 'serviceOfferForm', reducer });
  useInjectSaga({ key: 'serviceOfferForm', saga });

  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [id, setId] = useState(undefined);

  const steps = getSteps();

  useEffect(() => {
    const url = window.location.pathname.split('/');
    const urlId = url[2];
    if (urlId !== undefined) {
      setId(urlId);
      fetchEditOffer(urlId);
    }
  }, []);

  const submitForm = () => {
    if (id !== undefined) {
      updateOffer();
    } else {
      createOffer();
    }
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <RequirementServiceForm
            values={serviceOfferForm}
            actions={{ updateTextField }}
          />
        );
      case 1:
        return (
          <DropZoneServiceForm
            file={serviceOfferForm.file}
            action={uploadFile}
            deleteFile={deleteFile}
          />
        );
      default:
        return 'Unknown step';
    }
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <>
      <Typography variant="h4" className={classes.header}>
        Naujas atliekamos paslaugos skelbimas
      </Typography>
      <div className={classes.root}>
        <Stepper nonLinear activeStep={activeStep} className={classes.root}>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel classes={{ label: classes.notActive }}>
                {label}
              </StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed
              </Typography>
              <Button onClick={handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <div className={classes.instructions}>
                {getStepContent(activeStep)}
              </div>
              <div className={classes.actionsContainer}>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  Atgal
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={
                    activeStep === steps.length - 1 ? submitForm : handleNext
                  }
                >
                  {activeStep === steps.length - 1 ? 'Pabaigti' : 'Pirmyn'}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

ServiceOfferForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
  serviceOfferForm: makeSelectServiceOfferForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateTextField,
      uploadFile,
      deleteFile,
      createOffer,
      updateOffer,
      fetchEditOffer,
    },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ServiceOfferForm);
