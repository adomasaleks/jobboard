/*
 *
 * ServiceOfferForm constants
 *
 */

export const DEFAULT_ACTION = 'app/ServiceOfferForm/DEFAULT_ACTION';

export const UPDATE_TEXT_FIELD = 'app/OfferPage/UPDATE_TEXT_FIELD';
export const UPLOAD_FILE = 'app/OfferPage/UPLOAD_FILE';
export const DELETE_FILE = 'app/OfferPage/DELETE_FILE';

export const GET_SERVICE_OFFER = 'app/ServiceOfferForm/GET_SERVICE_OFFER';
export const GET_SERVICE_OFFER_SUCCESS =
  'app/ServiceOfferForm/GET_SERVICE_OFFER_SUCCESS';

export const CREATE_SERVICE_OFFER = 'app/ServiceOfferForm/CREATE_SERVICE_OFFER';
export const CREATE_SERVICE_OFFER_SUCCESS =
  'app/ServiceOfferForm/CREATE_SERVICE_OFFER_SUCCESS';

export const UPDATE_SERVICE_OFFER = 'app/ServiceOfferForm/UPDATE_SERVICE_OFFER';
export const UPDATE_SERVICE_OFFER_SUCCESS =
  'app/ServiceOfferForm/UPDATE_SERVICE_OFFER_SUCCESS';
