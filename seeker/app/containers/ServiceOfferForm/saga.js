import { call, put, select, takeLatest } from 'redux-saga/effects';
import history from 'utils/history';
import { fetchOffer, fetchPostOffer, updateOffer } from 'containers/API/offer';
import {
  CREATE_SERVICE_OFFER,
  CREATE_SERVICE_OFFER_SUCCESS,
  GET_SERVICE_OFFER,
  UPDATE_SERVICE_OFFER,
  UPDATE_SERVICE_OFFER_SUCCESS,
} from './constants';
import makeSelectServiceOfferForm from './selectors';
import { createOfferSuccess } from './actions';

function* getOffer() {
  const state = yield select(makeSelectServiceOfferForm());
  try {
    const res = yield call(fetchOffer, state.id);
    yield put(createOfferSuccess(res.data));
  } catch (e) {
    console.log(e);
  }
}

function* postOffer() {
  const state = yield select(makeSelectServiceOfferForm());
  try {
    yield call(fetchPostOffer, state);
    yield put({ type: CREATE_SERVICE_OFFER_SUCCESS });
    yield put(history.push('/'));
  } catch (e) {
    console.log(e);
  }
}

function* fetchUpdateOffer() {
  const state = yield select(makeSelectServiceOfferForm());
  try {
    yield call(updateOffer, state.id, state);
    yield put({ type: UPDATE_SERVICE_OFFER_SUCCESS });
    history.push('/');
  } catch (e) {
    console.log(e);
  }
}

// Individual exports for testing
export default function* serviceOfferFormSaga() {
  yield takeLatest(CREATE_SERVICE_OFFER, postOffer);
  yield takeLatest(GET_SERVICE_OFFER, getOffer);
  yield takeLatest(UPDATE_SERVICE_OFFER, fetchUpdateOffer);
}
