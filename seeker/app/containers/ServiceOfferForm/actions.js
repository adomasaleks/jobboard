/*
 *
 * ServiceOfferForm actions
 *
 */

import {
  UPDATE_TEXT_FIELD,
  UPLOAD_FILE,
  DELETE_FILE,
  CREATE_SERVICE_OFFER,
  UPDATE_SERVICE_OFFER,
  GET_SERVICE_OFFER,
  GET_SERVICE_OFFER_SUCCESS,
} from './constants';

export function updateTextField(fieldId, data) {
  return {
    type: UPDATE_TEXT_FIELD,
    fieldId,
    data,
  };
}

export function uploadFile(file) {
  return {
    type: UPLOAD_FILE,
    file,
  };
}
export function deleteFile() {
  return {
    type: DELETE_FILE,
  };
}

export function createOffer() {
  return {
    type: CREATE_SERVICE_OFFER,
  };
}
export function createOfferSuccess(data) {
  return {
    type: GET_SERVICE_OFFER_SUCCESS,
    data,
  };
}

export function fetchEditOffer(id) {
  return {
    type: GET_SERVICE_OFFER,
    id,
  };
}

export function updateOffer() {
  return {
    type: UPDATE_SERVICE_OFFER,
  };
}
