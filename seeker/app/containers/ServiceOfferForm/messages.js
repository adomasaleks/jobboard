/*
 * ServiceOfferForm Messages
 *
 * This contains all the text for the ServiceOfferForm container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ServiceOfferForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ServiceOfferForm container!',
  },
});
