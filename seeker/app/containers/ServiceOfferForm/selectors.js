import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the serviceOfferForm state domain
 */

const selectServiceOfferFormDomain = state =>
  state.serviceOfferForm || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ServiceOfferForm
 */

const makeSelectServiceOfferForm = () =>
  createSelector(
    selectServiceOfferFormDomain,
    substate => substate,
  );

export default makeSelectServiceOfferForm;
export { selectServiceOfferFormDomain };
