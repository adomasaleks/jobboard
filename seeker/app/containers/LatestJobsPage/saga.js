import { call, put, takeLatest } from 'redux-saga/effects';
import { fetchLatestJobs } from 'containers/API/jobs';
import { FETCH_LATEST_JOBS } from './constants';
import { fetchLatestJobsSuccess } from './actions';
function* fetchJobs() {
  try {
    const res = yield call(fetchLatestJobs);
    yield put(fetchLatestJobsSuccess(res.data));
  } catch (e) {
    console.log(e);
  }
}
// Individual exports for testing
export default function* latestJobsPageSaga() {
  yield takeLatest(FETCH_LATEST_JOBS, fetchJobs);
}
