/*
 * LatestJobsPage Messages
 *
 * This contains all the text for the LatestJobsPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.LatestJobsPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the LatestJobsPage container!',
  },
});
