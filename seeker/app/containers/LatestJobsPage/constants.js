/*
 *
 * LatestJobsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LatestJobsPage/DEFAULT_ACTION';

export const FETCH_LATEST_JOBS = 'app/LatestJobsPage/FETCH_LATEST_JOBS';
export const FETCH_LATEST_JOBS_SUCCESS =
  'app/LatestJobsPage/FETCH_LATEST_JOBS_SUCCESS';
export const FETCH_LATEST_JOBS_ERROR =
  'app/LatestJobsPage/FETCH_LATEST_JOBS_ERROR';
