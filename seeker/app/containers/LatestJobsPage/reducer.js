/*
 *
 * LatestJobsPage reducer
 *
 */
import produce from 'immer';
import { FETCH_LATEST_JOBS, FETCH_LATEST_JOBS_SUCCESS } from './constants';

export const initialState = [];

/* eslint-disable default-case, no-param-reassign */
const latestJobsPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_LATEST_JOBS:
        return initialState;
        break;
      case FETCH_LATEST_JOBS_SUCCESS:
        action.data.map(value => {
          draft.push({
            id: value.id,
            name: value.name,
            companyName: value.companyName,
            city: value.city,
            salaryFrom: value.salaryFrom,
            createdAt: value.createdAt,
          });
        });
        break;
    }
  });

export default latestJobsPageReducer;
