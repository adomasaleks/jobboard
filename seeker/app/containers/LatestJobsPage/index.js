/**
 *
 * LatestJobsPage
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
//import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

//Material-ui
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import Input from '@material-ui/core/Input';

//components
import JobsTable from 'components/JobsTable';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { fetchLatestJobs } from './actions';
import makeSelectLatestJobsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
//import messages from './messages';

const useStyles = makeStyles(theme => ({
  root: {
    alignItems: 'flex-start',
  },
  filterContainer: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
  textField: {
    color: '#ffffff',
    margin: 0,
    '& .MuiInput-root:before': {
      borderBottomColor: '#fff8', // Semi-transparent underline
    },
    '& .MuiInput-underline:hover:not(.Mui-disabled):before': {
      borderBottomColor: '#fff', // Solid underline on hover
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#fff', // Solid underline on focus
    },
  },
  underline: {
    '&:before': {
      borderBottomColor: '#fff8',
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      borderBottomColor: '#fff !important',
    },
  },
  datePicker: {
    margin: 0,
  },
}));

const cities = [
  {
    value: 'vilnius',
    label: 'Vilnius',
  },
  {
    value: 'kaunas',
    label: 'Kaunas',
  },
];

export function LatestJobsPage({ fetchLatestJobs, latestJobsPage }) {
  useInjectReducer({ key: 'latestJobsPage', reducer });
  useInjectSaga({ key: 'latestJobsPage', saga });

  const classes = useStyles();

  const [city, setCity] = useState(cities[0].value);
  const [selectedDate, setSelectedDate] = React.useState(
    new Date('2014-08-18T21:11:54'),
  );

  const handleDateChange = date => {
    setSelectedDate(date);
  };

  const handleChange = e => {
    setCity(e.target.value);
  };

  useEffect(() => {
    fetchLatestJobs();
  }, []);

  return (
    <div>
      <AppBar
        className={classes.root}
        position="static"
        style={{ backgroundColor: '#ECC30B' }}
      >
        <Toolbar className={classes.filterContainer}>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <FormControl>
            <InputLabel htmlFor="city">Miestas</InputLabel>
            <Select
              id="city"
              label="Miestas"
              value={city}
              onChange={handleChange}
              className={classes.textField}
              input={
                <Input
                  classes={{
                    underline: classes.underline,
                  }}
                />
              }
              IconComponent={() => <ArrowDropDownIcon />}
            >
              {cities.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            label="Atlygis nuo"
            id="salaryFrom"
            className={classes.textField}
            InputProps={{
              endAdornment: <InputAdornment position="start">€</InputAdornment>,
            }}
          />
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="Data nuo"
              onChange={handleDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
              className={classes.textField}
              InputProps={{ className: classes.textField }}
              keyboardIcon={<CalendarTodayIcon style={{ fill: '#ffffff' }} />}
            />
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="Data iki"
              onChange={handleDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
              className={classes.textField}
              InputProps={{ className: classes.textField }}
              keyboardIcon={<CalendarTodayIcon style={{ fill: '#ffffff' }} />}
            />
          </MuiPickersUtilsProvider>
          <Button style={{ color: '#ffffff' }}>Atsijungti</Button>
        </Toolbar>
      </AppBar>
      <JobsTable data={latestJobsPage} />
    </div>
  );
}

LatestJobsPage.propTypes = {
  fetchLatestJobs: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  latestJobsPage: makeSelectLatestJobsPage(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchLatestJobs }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LatestJobsPage);
