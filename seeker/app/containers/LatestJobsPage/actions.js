/*
 *
 * LatestJobsPage actions
 *
 */

import { FETCH_LATEST_JOBS, FETCH_LATEST_JOBS_SUCCESS } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function fetchLatestJobs() {
  return {
    type: FETCH_LATEST_JOBS,
  };
}
export function fetchLatestJobsSuccess(data) {
  return {
    type: FETCH_LATEST_JOBS_SUCCESS,
    data,
  };
}
