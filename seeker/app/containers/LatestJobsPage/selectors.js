import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the latestJobsPage state domain
 */

const selectLatestJobsPageDomain = state =>
  state.latestJobsPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LatestJobsPage
 */

const makeSelectLatestJobsPage = () =>
  createSelector(
    selectLatestJobsPageDomain,
    substate => substate,
  );

export default makeSelectLatestJobsPage;
export { selectLatestJobsPageDomain };
