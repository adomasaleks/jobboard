/*
 *
 * Auth actions
 *
 */

import { AUTH_SUCCESS } from './constants';

export function authSuccess(token, expTime) {
  return {
    type: AUTH_SUCCESS,
    token,
    expTime,
  };
}
