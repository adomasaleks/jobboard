/*
 *
 * Auth reducer
 *
 */
import produce from 'immer';
import { AUTH_SUCCESS } from './constants';
import isAuthenticated from 'utils/isAuthenticated';

export const initialState = {
  token: localStorage.getItem('token') || null,
  expTime: Number(localStorage.getItem('expTime')) || null,
  isAuthenticated: isAuthenticated(),
};

/* eslint-disable default-case, no-param-reassign */
const authReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case AUTH_SUCCESS:
        draft.token = action.token;
        draft.expTime = action.expTime;
        draft.isAuthenticated = true;
        break;
    }
  });

export default authReducer;
