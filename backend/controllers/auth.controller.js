const db = require("../models");
const config = require("../config/auth.config");
const User = db.users;
const Recruiter = db.recruiter;
const controller = require("../controllers/user.controller");
const Promise = require('promise');

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var crypto = require("crypto");
const { authJwt } = require("../middleware");
const { recruiter } = require("../models");

exports.signUp = (req, res) => {
  User.create({
    id: User.id,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    phoneNumber: req.body.phoneNumber,
  })
    .then((user) => {
      var token = jwt.sign({ id: user.id,email:user.email }, config.secret, {
        expiresIn: 86000, // 24 hours
      });
      if (user) {
        res.send({
          message: "User was registered successfully!",
          accessToken: token,
          expiresIn: 86400,
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ error: err.message });
    });
};

exports.recruiterSignUp = (req, res) => {
  Recruiter.create({
    id: Recruiter.id,
    companyName: req.body.companyName,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    phoneNumber: req.body.phoneNumber,
  })
    .then((recruiter) => {
      var token = jwt.sign({ id: recruiter.id }, config.secret, {
        expiresIn: 86400, // 24hours
      });
      if (recruiter) {
        res.send({
          message: "Recruiter was registered successfully",
          accessToken: token,
          expiresIn: 86400,
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(err.status).send({ message: err });
    });
};

exports.recruiterLogin = (req, res) => {
  Recruiter.findOne({
    where: {
      email: req.body.email,
    },
  }).then((recruiter) => {
    if (!recruiter) {
      return res.status(401).send({
        accessToken: null,
        message: "Invalid Password!",
      });
    }
    var token = jwt.sign({ id: recruiter.id }, config.secret, {
      expiresIn: 86400,
    });

    res.status(200).send({
      id: recruiter.id,
      email: recruiter.email,
      accessToken: token,
      expiresIn: 86400,
    });
  });
};
exports.recruiterforgotPassword = (req, res) => {
  //Create token
  const token = crypto.randomBytes(20).toString("hex");
  console.log("token", token);
  //find if recruiter exists
  User.findOne({
    where: {
      email: req.body.email,
    },
  })
    .then((recruiter) => {
      if (!recruiter) {
        return res.status(404).send({ error: "Recruiter not found" });
      }
      recruiter.resetPasswordToken = token;
      recruiter.resetPasswordExpires = Date.now() + 3600000; // 1 hour

      recruiter.save({
        fields: ["resetPasswordToken", "resetPasswordExpires"],
      });

      //send Email
      const transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        auth: {
          user: "karlie.wisozk3@ethereal.email",
          pass: "RfxyejQSNTmHY35dmR",
        },
      });
      var mailOptions = {
        to: user.email,
        from: "karlie.wisozk3@ethereal.email",
        subject: "Jobboard Password Reset",
        text:
          "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
          "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
          "http://localhost:3001/reset/" +
          token +
          "\n\n" +
          "If you did not request this, please ignore this email and your password will remain unchanged.\n",
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
        }
      });

      res.status(200).send({ message: "created" });
    })
    .catch((err) => {
      return res.status(500).send({ error: err });
    });
};

exports.recruiterResetPasswordTokenCheck = (req, res) => {
  User.findOne({
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        [Op.gt]: Date.now(),
      },
    },
  }).then((recruiter) => {
    if (!recruiter) {
      return res.status(404).send({ error: "Token not found!" });
    }
    return res.status(200).send({ message: "Success" });
  });
};

exports.recruiterSaveResetPassword = (req, res) => {
  User.findOne({
    where: {
      resetPasswordToken: req.body.token,
      resetPasswordExpires: {
        [Op.gt]: Date.now(),
      },
    },
  }).then((recruiter) => {
    if (!recruiter) {
      return res
        .status(404)
        .send({ error: "Password reset token is invalid or has expired." });
    }

    recruiter.password = bcrypt.hashSync(req.body.password, 8);
    recruiter.resetPasswordToken = null;
    recruiter.resetPasswordExpires = null;
    recruiter.save({
      fields: ["resetPasswordToken", "resetPasswordExpires", "password"],
    });

    return res.status(200).send({ message: "Password Updated" });
  });
};
// const token = req.headers.authorization.split(" ")[1];
// const decodedJwt = jwt.decode(token, { complete: true });
// let updateValues = { password: bcrypt.hashSync(req.body.newPassword, 8) };
// Recruiter.update(updateValues,{
//   where:{
//       id:decodedJwt.payload.id,
//   }
//  })
//   .then((recruiter)=>{
//     console.log(recruiter);
//     const res = bcrypt.compare(req.body.password, recruiter.password);
//     if(!res){
//        res.status(404).send({
//           message: "password is incorrect"
//       });
//   }else{
//       res.status(200).send({
//           message: "Recruiter profile was updated!"
//       })
//   }
// })
// .catch((err) => {
//   res.status(500).send({ error: err.message });
// });
exports.changePasswordRecruiter = (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  let updateValues = { password: bcrypt.hashSync(req.body.newPassword, 8) };

    Recruiter.update(updateValues,{
      where: {
        id: decodedJwt.payload.id,
      }
    })
    .then((recruiter)=>{
      console.log(recruiter.password);
    })

  }

exports.changePasswordSeeker = (req,res) => {
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  console.log(bcrypt.hashSync(req.body.password, 8) == user.password);
  console.log(user.password);
  console.log(bcrypt.hashSync(req.body.password, 8));
 if(bcrypt.hashSync(req.body.password, 8) == User.password){
    return console.log(true);
 }

}
function getSeekerbyId(email){
  
}

exports.signIn = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email,
    },
  }).then((user) => {
    if (!user) {
      return res.status(401).send({
        accessToken: null,
        message: "Invalid Password!",
      });
    }
    var token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 86400,
    });

    res.status(200).send({
      id: user.id,
      email: user.email,
      accessToken: token,
      expiresIn: 86400,
    });
  });
};

exports.updateProfileRecruiter =  (req,res) =>{
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  Recruiter.update(req.body,{
    id: decodedJwt.payload.id,
  })
    .then((recruiter) => {
      if (!recruiter) {
        res.status(401).send({
          message: "There was an error when updating recruiter profile!",
        });
      } else {
        res.status(200).send({
          message: "Recruiter profile was updated!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({ error: err.message });
    });
};
