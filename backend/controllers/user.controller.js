const db = require("../models");
const config = require("../config/auth.config");
const User = db.users;


exports.seekerForgotPassword = (req, res) => {
    //Create token
    const token = crypto.randomBytes(20).toString("hex");
    console.log("token", token);
    //find if recruiter exists
    User.findOne({

         where: {
             email: req.body.email
             }
     })
      .then((users) => {
        if (!users) {
          return res.status(404).send({ error: "User not found" });
        }
        users.resetPasswordToken = token;
        users.resetPasswordExpires = Date.now() + 3600000; // 1 hour
  
        users.save({ fields: ["resetPasswordToken", "resetPasswordExpires"] });
  
        //send Email
        const transporter = nodemailer.createTransport({
          host: "smtp.ethereal.email",
          port: 587,
          auth: {
            user: "karlie.wisozk3@ethereal.email",
            pass: "RfxyejQSNTmHY35dmR",
          },
        });
        var mailOptions = {
          to: user.email,
          from: "karlie.wisozk3@ethereal.email",
          subject: "Jobboard Password Reset",
          text:
            "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
            "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
            "http://localhost:3001/reset/" +
            token +
            "\n\n" +
            "If you did not request this, please ignore this email and your password will remain unchanged.\n",
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.log(error);
          }
        });
  
        res.status(200).send({ message: "created" });
      })
      .catch((err) => {
        return res.status(500).send({ error: err });
      });
  };

  exports.userResetPasswordTokenCheck = (req, res) => {
    User.findOne({
      where: {
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {
          [Op.gt]: Date.now(),
        },
      },
    }).then((users) => {
      if (!users) {
        return res.status(404).send({ error: "Token not found!" });
      }
      return res.status(200).send({ message: "Success" });
    });
  };

  exports.userSaveResetPassword = (req, res) => {
    User.findOne({
      where: {
        resetPasswordToken: req.body.token,
        resetPasswordExpires: {
          [Op.gt]: Date.now(),
        },
      },
    }).then((users) => {
      if (!users) {
        return res
          .status(404)
          .send({ error: "Password reset token is invalid or has expired." });
      }

      users.password = bcrypt.hashSync(req.body.password, 8);
      users.resetPasswordToken = null;
      users.resetPasswordExpires = null;
      users.save({
        fields: ["resetPasswordToken", "resetPasswordExpires", "password"],
      });

      return res.status(200).send({ message: "Password Updated" });
    });
  };
