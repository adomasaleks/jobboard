const db = require("../models");
const Cv = db.cv;
const jwt = require("jsonwebtoken");

exports.getCvBySeekerId = (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  Cv.findOne({
    where: {
      userId: decodedJwt.payload.id,
    },
  })
    .then((data) => {
      console.log(req.body);
      if (!data) {
        res.status(404).send("No cv found");
      }
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving cv with cvId=" + decodedJwt.payload.id,
      });
    });
};

exports.createCv = (req,res) => {
    const token = req.headers.authorization.split(" ")[1];
    const decodedJwt = jwt.decode(token, { complete: true });
    Cv.create({
        id: Cv.id,
        name: req.body.name,
        lastName: req.body.lastName,
        dateOfBirth: req.body.dateOfBirth,
        position: req.body.position,
        createdAt: req.body.createdAt,
        country: req.body.country,
        phoneNumber: req.body.phoneNumber,
        userId : decodedJwt.payload.id,
    })
    .then((cv) =>{
        if(!cv){
            res.status(401).send({
                message: "Cv was not created, check your mistakes!"
            });
        }else{
                res.send({
                    message: "Cv was successfully created!",
                  });
        }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ error: err.message });
    });
};

exports.getCvById = (req, res) => {
  Cv.findOne({
    where: {
      id: req.params.id,
    },
  })
    .then((data) => {
      console.log(req.body);
      if (!data) {
        res.status(404).send("No cv found");
      }
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving cv with cvId=" + cvId,
      });
    });
};

exports.updateCv = (req,res) => {
    const token = req.headers.authorization.split(" ")[1];
    const decodedJwt = jwt.decode(token, { complete: true });
    const id = req.params.cvId;
    Cv.update(req.body,{
            where:{
                id:id,
                userId:decodedJwt.payload.id
            }
           })
    .then((cv)=>{
          if(!cv){
            res.status(401).send({
                message: "There was an error when updating cv!"
            });
        }else{
            res.status(200).send({
                message: "Cv was updated!"
            })
        }
    })
    .catch((err) => {
      res.status(500).send({ error: err.message });
    });
};
exports.deleteCv = (req, res) => {
  const id = req.params.cvId;

    Cv.destroy({
        where:{
            id:id
        }
    })
    .then(num=>{
        if(num == 1){
            res.send({
                message: "Cv was deleted successfully"
              });
        }else{
            res.send({
                message: "Cannot delete cv!"
              });
        }
    })
    .catch((err) => {
      res.status(500).send({ error: err.message });
    });
};