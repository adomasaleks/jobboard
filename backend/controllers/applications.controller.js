const db = require("../models");
const { recruiter, offer } = require("../models");
const Offer = db.offer;
const Application = db.application;
const Cv = db.cv;
const User = db.users;


exports.sendApllication = (req,res) => {
    const offerid = req.params.offerId;
    const cvId = 12;
    Application.create({
        id: Application.id,
        createdAt: req.body.createdAt,
        offerId: offerid,
        cvId: cvId
    })
    .then((application)=>{
        if(!application){
            res.status(401).send({
                message: "Please select cv that you want to apply with!"
            });
        }
        res.status(200).send({
            message: "Application was sent!",
          });
    })
    .catch((err) => {
        res.status(500).send({ error: err.message });
      });
}

exports.getJobOffersApplicationsById = (req,res) => {
        const offerId = req.params.offerId;
    Application.findAll({
            where:{
                offerId:offerId
            },
            include:[{model: Cv,as:'cv'}]
        })
        .then(data => {
            console.log(req.body);
            if(!data){
                res.status(404).send("No applications found");
            }
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving applications with recruiterId=" + offerId
            });
        });
}
