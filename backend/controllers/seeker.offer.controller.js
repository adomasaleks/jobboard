const db = require("../models");
const { users, offer } = require("../models");
const SeekerOffer = db.seekerOffer;
const User = db.users;
const jwt = require("jsonwebtoken");

// change to seeker id from token
exports.findSeekerOffersById = (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  Offer.findAll({
    where: {
      userId: decodedJwt.payload.id,
    },
  })
    .then((data) => {
      console.log(req.body);
      if (!data) {
        res.status(404).send("No offer found");
      }
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error retrieving offers with recruiterId=" + decodedJwt.payload.id,
      });
    });
};

//add seekerid from token
exports.createSeekerOffer = (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  const decodedJwt = jwt.decode(token, { complete: true });
  Offer.create({
    ...req.body,
    userId: decodedJwt.payload.id,
  })
    .then((offer) => {
      if (!offer) {
        res.status(401).send({
          message: "Offer was not created, check your mistakes!",
        });
      } else {
        res.send({
          message: "Offer was successfully created!",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ error: err.message });
    });
};

exports.updateSeekerOffer = (req, res) => {
  const id = req.params.offerId;
  Offer.update(req.body, {
    where: {
      id: id,
    },
  })
    .then((offer) => {
      if (!offer) {
        res.status(401).send({
          message: "There was an error when updating offer!",
        });
      } else {
        res.status(200).send({
          message: "Offer was updated!",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ error: err.message });
    });
};

exports.deleteSeekerOffer = (req, res) => {
  const id = req.params.offerId;
  Offer.destroy({
    where: {
      id: id,
    },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Offer was deleted successfully!",
        });
      } else {
        res.send({
          message: "Cannot delete offer!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({ error: err.message });
    });
};

exports.getSeekerOfferById = (req, res) => {
  const id = req.params.offerId;
  Offer.findOne({
    where: {
      id: id,
    },
  })
    .then((data) => {
      console.log(req.body);
      if (!data) {
        res.status(404).send("No offer found");
      }
      res.send(data);
    })
      .catch(err => {
            res.status(500).send({
                message: "Error retrieving offer with id=" + 1
            });
        });
}

exports.getAllActiveSeekerOffers = (req, res) => {
  Offer.findAll({
    where: {
      status: 1,
    },
  })
    .then((data) => {
      console.log(req.body);
      if (!data) {
        res.status(404).send("No offers found");
      }
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving active  offers",
      });
    });
};

exports.uploadSeekerFile = (req, res) => {
  let file;
  let uploadPath;

  if (!req.files || Object.keys(req.files).length === 0) {
    res.status(400).send("No files were uploaded.");
    return;
  }

  console.log("req.files >>>", req.files); // eslint-disable-line

  file = req.files.file;

  uploadPath = __dirname + "/uploads/" + file.name;

  file.mv(uploadPath, function (err) {
    if (err) {
      return res.status(500).send(err);
    }
    res.send("File uploaded to " + uploadPath);
  });
};
