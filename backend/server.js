const express = require("express");
const bodyParser = require("body-parser");
const db = require("./models");
const fileUpload = require("express-fileupload");

const app = express();
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());

db.sequelize.sync({ force: false }).then(() => {
  console.log("Drop and re-sync db.");
});

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.json({});
  }
  next();
});

//routes
require("./routes/recruiter.routes")(app);
require("./routes/offer.routes")(app);
require("./routes/user.routes")(app);
require("./routes/cv.routes")(app);
require("./routes/application.routes")(app);
require("./routes/seeker.offer.routes")(app);

app.get("/", (req, res) => {
  res.json({ message: "welcome" });
});
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
