const db = require("../models");
const Recruiter = db.recruiter;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Email
  Recruiter.findOne({
    where: {
      email: req.body.email,
    },
  }).then((recruiter) => {
    if (recruiter) {
      res.status(401).send({ message: "Failed! Email is already in use!" });
      return;
    }

    next();
  });
};


const verifySignUp = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail,
};

module.exports = verifySignUp;
