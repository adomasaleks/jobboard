const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const verifySignUpUser = require("./verifySignUpUser");

module.exports = {
    authJwt,
    verifySignUp,
    verifySignUpUser
};