const db = require("../models");
const User = db.users;

checkDuplicateUsernameOrEmailUser = (req, res, next) => {
    // Email
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(users => {
        if (users) {
            res.status(400).send({
                message: "Failed! Email is already in use!"
            });
            return;
        }

        next();
    });
};


const verifySignUpUser = {
    checkDuplicateUsernameOrEmailUser: checkDuplicateUsernameOrEmailUser,
}

module.exports = verifySignUpUser;