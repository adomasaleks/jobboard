module.exports = (sequelize, Sequelize) => {
  const Offer = sequelize.define("offer", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
    },
    fullTime: {
      type: Sequelize.BOOLEAN,
    },
    workRequirements: {
      type: Sequelize.STRING,
    },
    workDescription: {
      type: Sequelize.STRING,
    },
    offerPosition: {
      type: Sequelize.STRING,
    },
    startDate: {
      type: Sequelize.DATE,
    },
    endDate: {
      type: Sequelize.DATE,
    },
    status: {
      type: Sequelize.BOOLEAN,
    },
    salaryFrom: {
      type: Sequelize.INTEGER,
    },
    salaryTo: {
      type: Sequelize.INTEGER,
    },
    companyName: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    schedule: {
      type: Sequelize.STRING,
    },
    payment: {
      type: Sequelize.STRING,
    },
    answers: {
      type: Sequelize.TEXT,
      get: function () {
        return JSON.parse(this.getDataValue("answers"));
      },
      set: function (value) {
        return this.setDataValue("answers", JSON.stringify(value));
      },
    },
    file: {
      type: Sequelize.TEXT,
      get: function () {
        return JSON.parse(this.getDataValue("file"));
      },
      set: function (value) {
        return this.setDataValue("file", JSON.stringify(value));
      },
    },
  });

  return Offer;
};
