const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const { dialect } = require("../config/db.config.js");

const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,

{

    host: config.HOST,
    dialect: config.dialect,
    operatorAliases: false,

    pool: {
        max: config.pool.max,
        min: config.pool.min,
        acquire: config.pool.acquire,
        idle: config.pool.idle
    }
}
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("../models/user.model.js")(sequelize, Sequelize);
db.recruiter = require("../models/recruiter.model.js")(sequelize, Sequelize);
db.offer = require("../models/offer.model.js")(sequelize, Sequelize);
db.cv = require("../models/cv.model")(sequelize,Sequelize);
db.application = require("../models/application.model")(sequelize,Sequelize);
db.seekerOffer = require("../models/seeker.offer.model")(sequelize,Sequelize);


db.offer.hasMany(db.application, {as:"application"});
db.application.belongsTo(db.offer,{
    foreignKey: "offerId",
    as:"offer",
});
db.application.belongsTo(db.cv,{
    foreignKey:"cvId",
    as:"cv"
});
db.users.hasMany(db.seekerOffer,{as:"seekerOffer"});
db.seekerOffer.belongsTo(db.users,{
    foreignKey:"userId",
    as:"users",
});
db.cv.belongsTo(db.users,{
    foreignKey: "userId",
    as: "users",
});

db.recruiter.hasMany(db.offer, { as: "offer" });
db.offer.belongsTo(db.recruiter, {
    foreignKey: "recruiterId",
    as: "recruiter",
  });
module.exports = db;