
module.exports = (sequelize, Sequelize) => {
    const Cv = sequelize.define("cv", {
            name: {
            type: Sequelize.STRING,
          },
          lastName:{
              type: Sequelize.STRING,
          },
          dateOfBirth: {
              type:Sequelize.DATE,
          },
          position: {
            type: Sequelize.TEXT,
            get: function() {
              return JSON.parse(this.getDataValue("position"));
            },
            set: function(value) {
              return this.setDataValue("position", JSON.stringify(value));
            }
          },
          education: {
            type: Sequelize.TEXT,
            get: function() {
              return JSON.parse(this.getDataValue("education"));
            },
            set: function(value) {
              return this.setDataValue("education", JSON.stringify(value));
            }
          },
          courses: {
            type: Sequelize.TEXT,
            get: function() {
              return JSON.parse(this.getDataValue("courses"));
            },
            set: function(value) {
              return this.setDataValue("courses", JSON.stringify(value));
            }
          },
          country: {
            type: Sequelize.STRING,
          },
          phoneNumber: {
            type: Sequelize.INTEGER,
          }
        });

        return Cv;
    };