module.exports = (sequelize, Sequelize) => {
    const Recruiter = sequelize.define("recruiter", {
      companyName:{
        type:Sequelize.STRING,
      },
        email: {
        type: Sequelize.STRING,
      },
      phoneNumber: {
          type:Sequelize.INTEGER,
      },
      password: {
        type: Sequelize.STRING,
      },
      resetPasswordToken: {
        type: Sequelize.STRING,
      },
      resetPasswordExpires: {
        type: Sequelize.DATE,
      },
      resetPasswordToken: {
        type: Sequelize.STRING,
      },
      resetPasswordExpires: {
        type: Sequelize.DATE,
      },
      name :{
        type: Sequelize.STRING,
      },
      lastName: {
        type: Sequelize.STRING,
      }
    });

    return Recruiter;
  };