module.exports = (sequelize, Sequelize) => {
    const SeekerOffer = sequelize.define("seekerOffer", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      title: {
        type: Sequelize.STRING,
      },
      fullTime: {
        type: Sequelize.BOOLEAN,
      },
      workDescription: {
        type: Sequelize.STRING,
      },
      offerPosition: {
        type: Sequelize.STRING,
      },
      startDate: {
        type: Sequelize.DATE,
      },
      endDate: {
        type: Sequelize.DATE,
      },
      status: {
        type: Sequelize.BOOLEAN,
      },
      city: {
        type: Sequelize.STRING,
      },
      country:{
        type: Sequelize.STRING,
      },
      file: {
        type: Sequelize.TEXT,
        get: function () {
          return JSON.parse(this.getDataValue("file"));
        },
        set: function (value) {
          return this.setDataValue("file", JSON.stringify(value));
        },
      },
    });

    return SeekerOffer;
  };
