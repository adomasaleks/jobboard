module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
            email: {
            type: Sequelize.STRING,
          },
          phoneNumber: {
              type:Sequelize.INTEGER,
          },
          password: {
            type: Sequelize.STRING,
          },
          resetPasswordToken: {
            type: Sequelize.STRING,
          },
          resetPasswordExpires: {
            type: Sequelize.DATE,
          }
        });

        return User;
    };