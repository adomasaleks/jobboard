const controller = require("../controllers/cv.controller");
const {authJwt,verifySignUp} = require("../middleware");


module.exports = function (app){
    const url = "/api";

      app.get(`${url}/get-cv/`, controller.getCvBySeekerId);
      app.post(`${url}/create-cv`, controller.createCv);
      app.put(`${url}/update-cv/:cvId`, controller.updateCv);
      app.delete(`${url}/delete-cv/:cvId`, controller.deleteCv);
}