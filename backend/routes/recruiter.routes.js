const controller = require("../controllers/auth.controller");
const {authJwt,verifySignUp} = require("../middleware");

module.exports = function (app){
    const url = "/api";

    app.post(
        `${url}/register-recruiter`,
        [
          verifySignUp.checkDuplicateUsernameOrEmail,
        ],
        controller.recruiterSignUp
      );
      app.post(`${url}/recruiter-signin`,
      [
        authJwt.verifyToken,
      ],
      controller.recruiterLogin);

      app.post(`${url}/forgot`, controller.recruiterforgotPassword);

      app.get(`${url}/reset/:token`, controller.recruiterResetPasswordTokenCheck);

      app.post(`${url}/reset`, controller.recruiterSaveResetPassword);

      app.put(`${url}/update-profile-recruiter`, controller.updateProfileRecruiter);

      app.put(`${url}/update-recruiter-password`, controller.changePasswordRecruiter);
}