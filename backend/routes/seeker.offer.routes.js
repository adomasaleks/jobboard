const controller = require("../controllers/seeker.offer.controller");
const {authJwt,verifySignUp} = require("../middleware");


module.exports = function (app){
    const url = "/api";

      app.get(`${url}/get-seeker-offers/:recruiterId`, controller.findSeekerOffersById);
      app.post(`${url}/create-seeker-offer`, controller.createSeekerOffer);
      app.put(`${url}/update-seeker-offer/:offerId`, controller.updateSeekerOffer);
      app.delete(`${url}/delete-seeker-offer/:offerId`, controller.deleteSeekerOffer);
      app.get(`${url}/get-seeker-offer/:offerId`, controller.getSeekerOfferById);
      app.get(`${url}/get-all-seeker-offers`, controller.getAllActiveSeekerOffers);
      app.post('/upload-seeker-offer', controller.uploadSeekerFile);
}