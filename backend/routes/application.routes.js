const controller = require("../controllers/applications.controller");
const {authJwt,verifySignUp} = require("../middleware");


module.exports = function (app){
    const url = "/api";

    app.post(`${url}/send-application/:offerId`, controller.sendApllication);
    app.get(`${url}/get-offer-applications/:offerId`, controller.getJobOffersApplicationsById);

}