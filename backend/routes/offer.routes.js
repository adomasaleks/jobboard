const controller = require("../controllers/offer.controller");
const {authJwt,verifySignUp} = require("../middleware");


module.exports = function (app){
    const url = "/api";

      app.get(`${url}/get-recruiter-offers/:recruiterId`, controller.findOffersById);
      app.post(`${url}/create-offer`, controller.createOffer);
      app.put(`${url}/update-offer/:offerId`, controller.updateOffer);
      app.delete(`${url}/delete-offer/:offerId`, controller.deleteOffer);
      app.get(`${url}/get-offer/:offerId`, controller.getOfferById);
      app.get(`${url}/get-all-offers`, controller.getAllActiveOffers);
      app.post('/upload', controller.uploadFile);
}