const controller = require("../controllers/user.controller");
const controllerAuth = require("../controllers/auth.controller");
const { authJwt, verifySignUpUser } = require("../middleware");

module.exports = function (app) {
  const url = "/api";

  app.post(`${url}/signin`, controllerAuth.signIn);

  app.post(
    `${url}/signup`,
    [verifySignUpUser.checkDuplicateUsernameOrEmailUser],
    controllerAuth.signUp
  );
  app.post(`${url}/forgot`, controller.seekerForgotPassword);

  app.get(`${url}/reset/:token`, controller.userResetPasswordTokenCheck);

  app.post(`${url}/reset`, controller.userSaveResetPassword);
  
  app.put(`${url}/update-seeker-password`, controllerAuth.changePasswordSeeker);
};
