/* eslint-disable no-shadow */
/**
 *
 * OfferPage
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import OfferJobForm from 'components/OfferJobForm';
import RequirementJobForm from 'components/RequirementJobForm';
import DropZoneJobForm from 'components/DropZoneJobForm';
import makeSelectOfferPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  updateTextField,
  updateAnswer,
  addNewAnswer,
  deleteAnswer,
  uploadFile,
  deleteFile,
  postOffer,
  fetchEditOffer,
  updateOffer,
} from './actions';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 90,
  },
  header: {
    marginBottom: 60,
  },
  root: {
    backgroundColor: 'transparent',
    transition: 'none',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(8),
    textAlign: 'end',
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  notActive: {
    color: 'rgba(0, 0, 0, 0.36)',
  },
  backButton: {
    marginRight: 15,
  },
}));

function getSteps() {
  return ['Darbo aprašymas', 'Reikalavimai pozicijai', 'Atvaizdavimas'];
}

export function OfferPage({
  offerPage,
  updateTextField,
  updateAnswer,
  addNewAnswer,
  deleteAnswer,
  uploadFile,
  deleteFile,
  postOffer,
  fetchEditOffer,
  updateOffer,
}) {
  useInjectReducer({ key: 'offerPage', reducer });
  useInjectSaga({ key: 'offerPage', saga });

  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [id, setId] = useState(undefined);

  const steps = getSteps();

  useEffect(() => {
    const url = window.location.pathname.split('/');
    const urlId = url[2];
    if (urlId !== undefined) {
      setId(urlId);
      fetchEditOffer(urlId);
    }
  }, []);

  const submitForm = () => {
    if (id !== undefined) {
      updateOffer();
    } else {
      postOffer();
    }
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <OfferJobForm
            offer={offerPage}
            actions={{
              updateTextField,
            }}
          />
        );
      case 1:
        return (
          <RequirementJobForm
            answers={offerPage.answers}
            actions={{
              updateTextField,
              updateAnswer,
              addNewAnswer,
              deleteAnswer,
            }}
          />
        );
      case 2:
        return (
          <DropZoneJobForm
            file={offerPage.file}
            action={uploadFile}
            deleteFile={deleteFile}
          />
        );
      default:
        return 'Unknown step';
    }
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Container className={classes.container}>
      <Grid container justify="center">
        <Grid item md={9}>
          <Typography variant="h4" className={classes.header}>
            Naujas darbo skelbimas
          </Typography>
          <div className={classes.root}>
            <Stepper nonLinear activeStep={activeStep} className={classes.root}>
              {steps.map((label, index) => (
                <Step key={label}>
                  <StepLabel classes={{ label: classes.notActive }}>
                    {label}
                  </StepLabel>
                </Step>
              ))}
            </Stepper>
            <div>
              {activeStep === steps.length ? (
                <div>
                  <Typography className={classes.instructions}>
                    All steps completed
                  </Typography>
                  <Button onClick={handleReset}>Reset</Button>
                </div>
              ) : (
                <div>
                  <div className={classes.instructions}>
                    {getStepContent(activeStep)}
                  </div>
                  <div className={classes.actionsContainer}>
                    <Button
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.backButton}
                    >
                      Atgal
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={
                        activeStep === steps.length - 1
                          ? submitForm
                          : handleNext
                      }
                    >
                      {activeStep === steps.length - 1 ? 'Pabaigti' : 'Pirmyn'}
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}

OfferPage.propTypes = {
  offerPage: PropTypes.object.isRequired,
  updateTextField: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  offerPage: makeSelectOfferPage(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateTextField,
      updateAnswer,
      addNewAnswer,
      deleteAnswer,
      uploadFile,
      deleteFile,
      postOffer,
      fetchEditOffer,
      updateOffer,
    },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(OfferPage);
