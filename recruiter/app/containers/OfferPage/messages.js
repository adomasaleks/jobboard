/*
 * OfferPage Messages
 *
 * This contains all the text for the OfferPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.OfferPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the OfferPage container!',
  },
});
