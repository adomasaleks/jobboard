/*
 *
 * OfferPage constants
 *
 */

export const UPDATE_TEXT_FIELD = 'app/OfferPage/UPDATE_TEXT_FIELD';
export const UPDATE_ANSWER = 'app/OfferPage/UPDATE_ANSWER';
export const ADD_NEW_ANSWER = 'app/OfferPage/ADD_NEW_ANSWER';
export const DELETE_ANSWER = 'app/OfferPage/DELETE_ANSWER';
export const UPLOAD_FILE = 'app/OfferPage/UPLOAD_FILE';
export const DELETE_FILE = 'app/OfferPage/DELETE_FILE';

export const POST_OFFER = 'app/OfferPage/POST_OFFER';
export const POST_OFFER_SUCCESS = 'app/OfferPage/POST_OFFER_SUCCESS';
export const POST_OFFER_ERROR = 'app/OfferPage/POST_OFFER_ERROR';

export const FETCH_EDIT_OFFER = 'app/OfferPage/FETCH_EDIT_OFFER';
export const FETCH_EDIT_OFFER_SUCCESS =
  'app/OfferPage/FETCH_EDIT_OFFER_SUCCESS';
export const POST_EDIT_OFFER = 'app/OfferPage/EDIT_OFFER';
export const UPDATE_OFFER = 'app/OfferPage/UPDATE_OFFER';
export const UPDATE_OFFER_SUCCESS = 'app/OfferPage/UPDATE_OFFER_SUCCESS';
