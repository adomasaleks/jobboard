/*
 *
 * OfferPage reducer
 *
 */
import produce from 'immer';
import {
  UPDATE_TEXT_FIELD,
  UPDATE_ANSWER,
  ADD_NEW_ANSWER,
  DELETE_ANSWER,
  UPLOAD_FILE,
  DELETE_FILE,
  POST_OFFER_SUCCESS,
  FETCH_EDIT_OFFER,
  FETCH_EDIT_OFFER_SUCCESS,
} from './constants';

const payments = [
  {
    value: 'monthly',
    label: ' Per mėnesį',
  },
  {
    value: 'week',
    label: 'Per savaitę',
  },
];

const taxes = [
  {
    value: 'before',
    label: 'Prieš mokesčius',
  },
  {
    value: 'after',
    label: 'Po mokesčių',
  },
];

export const initialState = {
  id: '',
  payment: payments[0].value,
  tax: taxes[0].value,
  payments,
  taxes,
  fullTime: true,
  name: '',
  areaOfWork: '',
  workDescription: '',
  salaryForm: '',
  salaryTo: '',
  companyName: '',
  showCompanyName: '',
  city: '',
  address: '',
  schedule: '',
  workRequirements: '',
  answers: [''],
  file: [],
  errorMessage: '',
};

/* eslint-disable default-case, no-param-reassign */
const offerPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_TEXT_FIELD:
        draft[action.fieldId] = action.data;
        break;
      case UPDATE_ANSWER:
        draft.answers[action.id] = action.answer;
        break;
      case ADD_NEW_ANSWER:
        draft.answers.push('');
        break;
      case DELETE_ANSWER:
        draft.answers.splice(action.id, 1);
        break;
      case UPLOAD_FILE:
        draft.file = action.file;
        break;
      case DELETE_FILE:
        draft.file = [];
        break;
      case POST_OFFER_SUCCESS:
        return initialState;
      case FETCH_EDIT_OFFER:
        draft.id = action.id;
        break;
      case FETCH_EDIT_OFFER_SUCCESS:
        return { ...action.data, payments, taxes };
    }
  });

export default offerPageReducer;
