/*
 *
 * OfferPage actions
 *
 */

import {
  UPDATE_TEXT_FIELD,
  UPDATE_ANSWER,
  ADD_NEW_ANSWER,
  DELETE_ANSWER,
  UPLOAD_FILE,
  DELETE_FILE,
  POST_OFFER,
  POST_OFFER_SUCCESS,
  POST_OFFER_ERROR,
  FETCH_EDIT_OFFER,
  FETCH_EDIT_OFFER_SUCCESS,
  UPDATE_OFFER,
  UPDATE_OFFER_SUCCESS,
} from './constants';

export function updateTextField(fieldId, data) {
  return {
    type: UPDATE_TEXT_FIELD,
    fieldId,
    data,
  };
}
export function updateAnswer(id, answer) {
  return {
    type: UPDATE_ANSWER,
    id,
    answer,
  };
}
export function addNewAnswer() {
  return {
    type: ADD_NEW_ANSWER,
  };
}
export function deleteAnswer(id) {
  return {
    type: DELETE_ANSWER,
    id,
  };
}
export function uploadFile(file) {
  return {
    type: UPLOAD_FILE,
    file,
  };
}
export function deleteFile() {
  return {
    type: DELETE_FILE,
  };
}
export function postOffer() {
  return {
    type: POST_OFFER,
  };
}
export function postOfferSuccess() {
  return {
    type: POST_OFFER_SUCCESS,
  };
}
export function postOfferError(message) {
  return {
    type: POST_OFFER_ERROR,
    message,
  };
}

export function fetchEditOffer(id) {
  return {
    type: FETCH_EDIT_OFFER,
    id,
  };
}
export function fetchEditOfferSuccess(data) {
  return {
    type: FETCH_EDIT_OFFER_SUCCESS,
    data,
  };
}
export function updateOffer() {
  return {
    type: UPDATE_OFFER,
  };
}
export function updateOfferSuccess() {
  return {
    type: UPDATE_OFFER_SUCCESS,
  };
}
