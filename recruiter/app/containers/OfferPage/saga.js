import { takeLatest, call, put, select } from 'redux-saga/effects';
import history from 'utils/history';
import makeSelectAuth from 'containers/Auth/selectors';
import { postOffer, fetchEditOffer, updateOffer } from 'containers/API/offer';
import has from 'lodash/has';
import { POST_OFFER, FETCH_EDIT_OFFER, UPDATE_OFFER } from './constants';
import {
  postOfferSuccess,
  postOfferError,
  fetchEditOfferSuccess,
  updateOfferSuccess,
} from './actions';
import makeSelectOfferPage from './selectors';

function* postRecruiterOffer() {
  const state = yield select(makeSelectOfferPage());
  const auth = yield select(makeSelectAuth());
  const stateWithToken = { ...state, token: auth.token };
  console.log(stateWithToken);
  try {
    yield call(postOffer, stateWithToken);
    yield put(postOfferSuccess());
    yield put(history.push('/panel'));
  } catch (e) {
    if (has(e.response, 'data')) {
      yield put(postOfferError(e.response.data.message));
    }
  }
}

function* getOffer() {
  const state = yield select(makeSelectOfferPage());

  try {
    const res = yield call(fetchEditOffer, state.id);
    yield put(fetchEditOfferSuccess(res.data));
  } catch (e) {
    console.log(e);
  }
}

function* fetchUpdateOffer() {
  const state = yield select(makeSelectOfferPage());
  try {
    yield call(updateOffer, state.id, state);
    yield put(updateOfferSuccess());
    history.push('/panel');
  } catch (e) {
    console.log(e);
  }
}

// Individual exports for testing
export default function* offerPageSaga() {
  yield takeLatest(POST_OFFER, postRecruiterOffer);
  yield takeLatest(FETCH_EDIT_OFFER, getOffer);
  yield takeLatest(UPDATE_OFFER, fetchUpdateOffer);
}
