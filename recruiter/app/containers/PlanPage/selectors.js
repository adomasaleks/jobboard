import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the planPage state domain
 */

const selectPlanPageDomain = state => state.planPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PlanPage
 */

const makeSelectPlanPage = () =>
  createSelector(
    selectPlanPageDomain,
    substate => substate,
  );

export default makeSelectPlanPage;
export { selectPlanPageDomain };
