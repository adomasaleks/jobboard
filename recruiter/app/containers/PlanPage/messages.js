/*
 * PlanPage Messages
 *
 * This contains all the text for the PlanPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.PlanPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PlanPage container!',
  },
});
