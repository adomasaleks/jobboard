/*
 *
 * PlanPage reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION } from './constants';

export const initialState = [
  {
    cost: '100',
    plan: 'Neribotas',
    body: ['Neribotas skelbimų įkėlimas'],
  },
  {
    cost: '59.99',
    plan: 'Bazinis',
    body: ['10 skelbimų įkėlimų'],
  },
];

/* eslint-disable default-case, no-param-reassign */
const planPageReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
    }
  });

export default planPageReducer;
