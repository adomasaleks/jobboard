/**
 *
 * PlanPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
//Material-ui
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//Components
import Plan from 'components/Plan';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPlanPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  container: {
    paddingTop: theme.spacing(11),
    paddingBottom: 300,
    backgroundColor: '#F8F8F8',
  },
  subtitle: {
    marginTop: theme.spacing(7),
    marginBottom: theme.spacing(8),
  },
  divider: {
    backgroundColor: '#77A6B6',
    width: '100%',
    height: 64,
  },
}));

export function PlanPage({ planPage }) {
  useInjectReducer({ key: 'planPage', reducer });
  useInjectSaga({ key: 'planPage', saga });
  const classes = useStyles();
  return (
    <div>
      <div className={classes.divider} />
      <Container className={classes.container}>
        <Grid container justify="center">
          <Grid item md={10}>
            <Typography variant="h3">
              Pasirinkite Jums tinkamą paslaugą
            </Typography>
            <Typography variant="body1" className={classes.subtitle}>
              Planai
            </Typography>
            <Grid container spacing={8}>
              {planPage.map((current, id, value) => (
                <Grid item md={5} key={id}>
                  <Plan values={value[id]} />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

PlanPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  planPage: makeSelectPlanPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(PlanPage);
