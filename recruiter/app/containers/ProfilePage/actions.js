/*
 *
 * ProfilePage actions
 *
 */

import { updateInfo, updateRecruiterPassword } from 'containers/API/recruiter';
import history from 'utils/history';
import { UPDATE_PROFILE_INFO, UPDATE_PASSWORD } from './constants';

export function updateProfileInfo(name, number, email) {
  updateInfo(name, number, email);
  history.push('/panel');
  return {
    type: UPDATE_PROFILE_INFO,
  };
}
export function updatePassword(password) {
  updateRecruiterPassword(password);
  history.push('/panel');
  return {
    type: UPDATE_PASSWORD,
  };
}
