/*
 *
 * ProfilePage constants
 *
 */

export const UPDATE_PROFILE_INFO = 'app/ProfilePage/UPDATE_PROFILE_INFO';
export const UPDATE_PASSWORD = 'app/ProfilePage/UPDATE_PASSWORD';
