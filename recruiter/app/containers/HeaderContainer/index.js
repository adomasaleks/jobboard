/**
 *
 * HeaderContainer
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Header from 'components/Header';
import makeSelectAuth from 'containers/Auth/selectors';
import { createStructuredSelector } from 'reselect';
import isAuthenticated from 'utils/isAuthenticated';

export function HeaderContainer({ auth }) {
  return <Header isAuth={auth.isAuthenticated} />;
}

HeaderContainer.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(HeaderContainer);
