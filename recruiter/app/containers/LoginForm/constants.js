/*
 *
 * LoginForm constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginForm/DEFAULT_ACTION';

export const LOGIN_FETCH = 'app/LoginForm/LOGIN_FETCH';
export const LOGIN_SUCCESS = 'app/LoginForm/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'app/LoginForm/LOGIN_ERROR';
