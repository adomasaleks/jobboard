/**
 *
 * LoginForm
 *
 */

import React, { useState } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import makeSelectLoginForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import { loginRecruiter } from './actions';
import history from 'utils/history';

const useStyles = makeStyles(() => ({
  modal: {
    position: 'fixed',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.2)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    zIndex: 2,
  },
  paper: {
    padding: '60px 90px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  text: {
    marginBottom: 50,
  },
  textFieldContainer: {
    marginBottom: 30,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
}));

export function LoginForm({ location, loginRecruiter }) {
  useInjectReducer({ key: 'loginForm', reducer });
  useInjectSaga({ key: 'loginForm', saga });
  const params = new URLSearchParams(location.search);
  const classes = useStyles();
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  const onSubmit = () => {
    if (email && password) {
      console.log('WORKS');
      loginRecruiter(email, password);
    }
  };
  const handleClose = () => {
    history.push('/');
  };

  return createPortal(
    params.get('login') && (
      <Dialog open onClose={handleClose}>
        <Paper elevation={3} className={classes.paper}>
          <Typography variant="h3" align="center" className={classes.text}>
            Prisijunkite
          </Typography>
          <TextField
            fullWidth
            id="email"
            label="El. paštas"
            variant="outlined"
            type="email"
            InputLabelProps={{
              className: classes.textField,
            }}
            className={classes.textFieldContainer}
            onChange={e => setEmail(e.target.value)}
          />
          <TextField
            fullWidth
            id="password"
            label="Slaptaždis"
            variant="outlined"
            type="password"
            InputLabelProps={{
              className: classes.textField,
            }}
            className={classes.textFieldContainer}
            onChange={e => setPassword(e.target.value)}
          />
          <Button color="primary" variant="contained" onClick={onSubmit}>
            Prisijungti
          </Button>
        </Paper>
      </Dialog>
    ),
    document.getElementById('modal_app'),
  );
}

LoginForm.propTypes = {
  location: PropTypes.object,
  loginRecruiter: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginForm: makeSelectLoginForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ loginRecruiter }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LoginForm);
