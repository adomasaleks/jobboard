/*
 *
 * LoginForm reducer
 *
 */
import produce from 'immer';
import { LOGIN_FETCH, LOGIN_SUCCESS, LOGIN_ERROR } from './constants';

export const initialState = {
  email: '',
  password: '',
  errorMessage: '',
};

/* eslint-disable default-case, no-param-reassign */
const loginFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOGIN_FETCH:
        draft.email = action.email;
        draft.passsword = action.password;
        break;
      case LOGIN_SUCCESS:
        draft.email = '';
        draft.passsword = '';
        break;
      case LOGIN_ERROR:
        draft.email = '';
        draft.password = '';
        draft.errorMessage = action.message;
        break;
    }
  });

export default loginFormReducer;
