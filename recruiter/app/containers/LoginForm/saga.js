import { takeLatest, call, put, select } from 'redux-saga/effects';
import { LoginRecruiter } from 'containers/API/recruiter';
import history from 'utils/history';
import has from 'lodash/has';
import makeSelectLoginForm from './selectors';
import { loginRecruiterSuccess, loginRecruiterError } from './actions';
import { LOGIN_FETCH } from './constants';

// Individual exports for testing
function* fetchLogin() {
  const state = yield select(makeSelectLoginForm());
  console.log(state);
  try {
    console.log(state);
    const res = yield call(LoginRecruiter, state);
    yield put(loginRecruiterSuccess());
    localStorage.setItem('token', res.data.accessToken);
    localStorage.setItem('expTime', res.data.expiresIn);
    yield put(history.push('/panel'));
  } catch (e) {
    if (has(e.response, 'data')) {
      yield put(loginRecruiterError(e.response.data.message));
    }
  }
}

export default function* loginFormSaga() {
  yield takeLatest(LOGIN_FETCH, fetchLogin);
}
