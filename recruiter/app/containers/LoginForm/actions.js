/*
 *
 * LoginForm actions
 *
 */

import { LOGIN_FETCH, LOGIN_SUCCESS, LOGIN_ERROR } from './constants';

export function loginRecruiter(email, password) {
  return {
    type: LOGIN_FETCH,
    email,
    password,
  };
}

export function loginRecruiterSuccess() {
  return {
    type: LOGIN_SUCCESS,
  };
}

export function loginRecruiterError(message) {
  return {
    type: LOGIN_ERROR,
    message,
  };
}
