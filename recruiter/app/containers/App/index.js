/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Auth from 'containers/Auth';
import HomePage from 'containers/HomePage/Loadable';
import PanelPage from 'containers/PanelPage';
import OfferPage from 'containers/OfferPage';
import ProfilePage from 'containers/ProfilePage';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import PrivateRoute from 'containers/PrivateRoute';
import LoginForm from 'containers/LoginForm';
import PlanPage from 'containers/PlanPage';
import ErrorHandling from 'containers/ErrorHandling';

import '../../global-styles';
import HeaderContainer from 'containers/HeaderContainer';
export default function App() {
  return (
    <div>
      <Auth />
      <ErrorHandling />
      <HeaderContainer />
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route exact path="/panel" component={PanelPage} />
        <Route exact path="/offer" component={OfferPage} />
        <Route exact path="/offer/:id" component={OfferPage} />
        <Route exact path="/profile" component={ProfilePage} />
        <Route exact path="/plan" component={PlanPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <Route path="/" component={LoginForm} />
    </div>
  );
}
