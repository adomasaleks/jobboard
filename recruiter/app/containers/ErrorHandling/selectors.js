import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the errorHandling state domain
 */

const selectErrorHandlingDomain = state => state.errorHandling || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ErrorHandling
 */

const makeSelectErrorHandling = () =>
  createSelector(
    selectErrorHandlingDomain,
    substate => substate,
  );

export default makeSelectErrorHandling;
export { selectErrorHandlingDomain };
