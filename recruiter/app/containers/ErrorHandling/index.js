/**
 *
 * ErrorHandling
 *
 */

import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

//Material-ui
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import { useInjectReducer } from 'utils/injectReducer';
import makeSelectErrorHandling from './selectors';
import reducer from './reducer';

export function ErrorHandling({ errorHandling }) {
  useInjectReducer({ key: 'errorHandling', reducer });
  const first = useRef(true);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (first.current) {
      first.current = false;
    } else if (errorHandling.message !== '') {
      setIsOpen(true);
      const timer = setTimeout(() => {
        setIsOpen(false);
        console.log('Works');
      }, 4000);
      return () => clearTimeout(timer);
    } else {
      setIsOpen(false);
    }
  }, [errorHandling.message]);
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <Snackbar
      open={isOpen}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
    >
      <Alert severity="error">{errorHandling.message}</Alert>
    </Snackbar>
  );
}

ErrorHandling.propTypes = {
  errorHandling: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  errorHandling: makeSelectErrorHandling(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ErrorHandling);
