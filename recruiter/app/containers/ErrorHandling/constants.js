/*
 *
 * ErrorHandling constants
 *
 */

export const DELETE_ERROR = 'app/ErrorHandling/DELETE_ERROR';
export const SHOW_ERROR = 'app/ErrorHandling/SHOW_ERROR';
