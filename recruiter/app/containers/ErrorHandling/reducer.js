/*
 *
 * ErrorHandling reducer
 *
 */
import produce from 'immer';
import { SHOW_ERROR, DELETE_ERROR } from './constants';

export const initialState = {
  message: '',
};

/* eslint-disable default-case, no-param-reassign */
const errorHandlingReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SHOW_ERROR:
        draft.message = action.message;
        break;
      case DELETE_ERROR:
        draft.message = '';
        break;
    }
  });

export default errorHandlingReducer;
