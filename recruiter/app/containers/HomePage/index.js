/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import HomeHeroSection from '../../components/HomeHeroSection';
import HomePlans from '../../components/HomePlans';
import HomeContact from '../../components/HomeContact';
import HomeRegister from '../../components/HomeRegister';
import Footer from '../../components/Footer';
export default function HomePage() {
  return (
    <div>
      <HomeHeroSection />
      <HomePlans />
      <HomeContact />
      <HomeRegister />
      <Footer />
    </div>
  );
}
