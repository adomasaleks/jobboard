import { call, put, select, takeLatest } from 'redux-saga/effects';
import { createRecruiter } from 'containers/API/recruiter';
import { authSuccess } from 'containers/Auth/actions';
import history from 'utils/history';
import { SHOW_ERROR, DELETE_ERROR } from 'containers/ErrorHandling/constants';
import { CREATE_RECRUITER, CREATE_RECRUITER_ERROR } from './constants';
import makeSelectRegisterForm from './selectors';
import { createRecruiterSuccess } from './actions';

function* fetchCreateRecruiter() {
  const state = yield select(makeSelectRegisterForm());
  yield put({ type: DELETE_ERROR });
  try {
    const res = yield call(createRecruiter, state);
    yield put(createRecruiterSuccess());
    localStorage.setItem('token', res.data.accessToken);
    localStorage.setItem(
      'expTime',
      new Date().getTime() / 1000 + res.data.expiresIn,
    );
    yield put(authSuccess(res.data.accessToken, res.data.expiresIn));
    yield put(history.push('/panel'));
  } catch (e) {
    yield put({ type: CREATE_RECRUITER_ERROR });
    yield put({ type: SHOW_ERROR, message: e.message });
  }
}
// Individual exports for testing
export default function* registerFormSaga() {
  yield takeLatest(CREATE_RECRUITER, fetchCreateRecruiter);
}
