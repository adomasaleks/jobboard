/**
 *
 * RegisterForm
 *
 */

import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import ColorButton from '../../components/ColorButton';
import makeSelectRegisterForm from './selectors';
import reducer from './reducer';
import saga from './saga';
import { createRecruiter } from './actions';

const useStyles = makeStyles(() => ({
  textFieldContainer: {
    marginBottom: 10,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  button: {
    marginRight: 25,
  },
  text: {
    color: 'rgba(0, 0, 0, 0.54)',
  },
}));

function validation(values) {
  let errors = {
    mail: '',
    password: '',
  };
  if (!RegExp('[1-9]').test(values.password)) {
    errors.password = 'Slaptažodis turi turėti skaičius';
  } else if (!RegExp('[A-Z]').test(values.password)) {
    errors.password = 'Slaptažodis turi turėti didžiąją raidę';
  } else if (!RegExp('[!@#$%^&*(),.?":{}|<>]').test(values.password)) {
    errors.password = 'Slaptažodis turi turėti specialų simbolį';
  }
  return errors;
}

export function RegisterForm({ createRecruiter, registerForm }) {
  useInjectReducer({ key: 'registerForm', reducer });
  useInjectSaga({ key: 'registerForm', saga });
  const firstRender = useRef(true);

  //state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [mobile, setMobile] = useState('');
  const [company, setCompany] = useState('');
  const [companyCode, setCompanyCode] = useState('');
  const [error, setError] = useState({
    mail: '',
    password: '',
  });

  const classes = useStyles();
  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false; // it's no longer the first render
      return; // skip the code below
    }
    if (error.mail === '' && error.password === '') {
      createRecruiter({ email, password, name, mobile, company, companyCode });
    }
  }, [error]);
  const onSubmit = () => {
    setError(validation({ email, password }));
    //console.log(error);
  };

  return (
    <div>
      <form autoComplete="off">
        <Grid container>
          <Grid container spacing={8} className={classes.textFieldContainer}>
            <Grid item md={4}>
              <TextField
                fullWidth
                id="email"
                label="El. paštas"
                variant="outlined"
                type="email"
                InputLabelProps={{
                  className: classes.textField,
                }}
                onChange={e => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <TextField
                error={!!error.password}
                fullWidth
                id="password"
                label="Slaptažodis"
                type="password"
                variant="outlined"
                InputLabelProps={{
                  className: classes.textField,
                }}
                helperText={error.password}
                onChange={e => setPassword(e.target.value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={8} className={classes.textFieldContainer}>
            <Grid item md={4}>
              <TextField
                fullWidth
                id="name"
                label="Jūsų vardas"
                variant="outlined"
                InputLabelProps={{
                  className: classes.textField,
                }}
                onChange={e => setName(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <TextField
                fullWidth
                id="mobile"
                label="Telefono numeris"
                variant="outlined"
                InputLabelProps={{
                  className: classes.textField,
                }}
                onChange={e => setMobile(e.target.value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={8} className={classes.textFieldContainer}>
            <Grid item md={4}>
              <TextField
                fullWidth
                id="company"
                label="Įmonės pavadinimas"
                variant="outlined"
                placeholder="out"
                InputLabelProps={{
                  className: classes.textField,
                }}
                onChange={e => setCompany(e.target.value)}
              />
            </Grid>
            <Grid item md={4}>
              <TextField
                fullWidth
                id="companyCode"
                label="Įmonės kodas"
                variant="outlined"
                InputLabelProps={{
                  className: classes.textField,
                }}
                onChange={e => setCompanyCode(e.target.value)}
              />
            </Grid>
          </Grid>
        </Grid>
        <div>
          <ColorButton
            message="REGISTRUOTIS"
            backgroundColor="#4D7298"
            className={classes.button}
            onClick={onSubmit}
          />
          <Typography
            variant="subtitle2"
            component="span"
            className={classes.text}
          >
            Turite paskyra? Prisijunkite
          </Typography>
        </div>
        <div>
          <Checkbox
            color="secondary"
            checked={false}
            icon={
              <CheckBoxOutlineBlankIcon
                style={{ color: 'rgba(0, 0, 0, 0.54)' }}
              />
            }
            classes={{ root: classes.icon }}
            inputProps={{ 'aria-label': 'secondary checkbox' }}
          />
          <Typography
            variant="subtitle2"
            component="span"
            className={classes.text}
          >
            sutinku su puslapio sąlygomis ir taisyklėmis
          </Typography>
        </div>
      </form>
    </div>
  );
}

RegisterForm.propTypes = {
  createRecruiter: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  registerForm: makeSelectRegisterForm(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createRecruiter }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(RegisterForm);
