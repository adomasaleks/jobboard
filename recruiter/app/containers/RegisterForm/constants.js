/*
 *
 * RegisterForm constants
 *
 */

export const DEFAULT_ACTION = 'app/RegisterForm/DEFAULT_ACTION';

export const CREATE_RECRUITER = 'app/RegisterForm/CREATE_RECRUITER';

export const CREATE_RECRUITER_SUCCESS =
  'app/RegisterForm/CREATE_RECRUITER_SUCCESS';

export const CREATE_RECRUITER_ERROR = 'app/RegisterForm/CREATE_RECRUITER_ERROR';
