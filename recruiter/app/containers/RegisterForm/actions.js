/*
 *
 * RegisterForm actions
 *
 */

import {
  CREATE_RECRUITER,
  CREATE_RECRUITER_SUCCESS,
  CREATE_RECRUITER_ERROR,
} from './constants';

export function createRecruiter({
  email,
  password,
  name,
  mobile,
  company,
  companyCode,
}) {
  return {
    type: CREATE_RECRUITER,
    email,
    password,
    name,
    mobile,
    company,
    companyCode,
  };
}

export function createRecruiterSuccess() {
  return {
    type: CREATE_RECRUITER_SUCCESS,
  };
}
