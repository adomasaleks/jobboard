/*
 *
 * RegisterForm reducer
 *
 */
import produce from 'immer';
import {
  CREATE_RECRUITER,
  CREATE_RECRUITER_SUCCESS,
  CREATE_RECRUITER_ERROR,
} from './constants';

export const initialState = {
  email: '',
  password: '',
  name: '',
  mobile: '',
  company: '',
  companyCode: '',
};

/* eslint-disable default-case, no-param-reassign */
const registerFormReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CREATE_RECRUITER:
        draft.email = action.email;
        draft.password = action.password;
        draft.name = action.name;
        draft.mobile = action.mobile;
        draft.company = action.company;
        draft.companyCode = action.companyCode;
        break;
      case CREATE_RECRUITER_SUCCESS:
        draft = { ...initialState };
        break;
      case CREATE_RECRUITER_ERROR:
        draft = { ...initialState };
        break;
    }
  });

export default registerFormReducer;
