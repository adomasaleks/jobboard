import axios from 'axios';
import { authHeader } from 'utils/authHeader';
export function createRecruiter({
  email,
  password,
  name,
  mobile,
  company,
  companyCode,
}) {
  return axios.post('http://localhost:3001/api/register-recruiter', {
    email,
    password,
    name,
    phoneNumber: mobile,
    companyName: company,
    companyCode,
  });
}

export function LoginRecruiter({ email, password }) {
  return axios.post('http://localhost:3001/api/recruiter-signin', {
    email,
    password,
  });
}

export function updateInfo(name, mobile, email) {
  return axios.put(
    'http://localhost:3001/api/update-profile-recruiter',
    {
      name,
      mobile,
      email,
    },
    {
      headers: authHeader(),
    },
  );
}

export function updateRecruiterPassword(newPassword) {
  return axios.put(
    'http://localhost:3001/api/update-recruiter-password',
    { newPassword },
    {
      headers: authHeader(),
    },
  );
}
