import axios from 'axios';
import { authHeader } from 'utils/authHeader';
export function postOffer(state) {
  return axios.post('http://localhost:3001/api/create-offer', state, {
    headers: authHeader(),
  });
}
export function fetchOffer() {
  console.log(authHeader());
  return axios.get('http://localhost:3001/api/get-recruiter-offers/2', {
    headers: authHeader(),
  });
}
export function fetchEditOffer(id) {
  return axios.get(`http://localhost:3001/api/get-offer/${id}`);
}
export function updateOffer(id, data) {
  return axios.put(`http://localhost:3001/api/update-offer/${id}`, data);
}
export function fetchDeleteOffer(id) {
  return axios.delete(`http://localhost:3001/api/delete-offer/${id}`);
}
