/*
 *
 * PanelPage reducer
 *
 */
import produce from 'immer';
import {
  FETCH_USER_OFFER,
  FETCH_USER_OFFER_SUCCESS,
  FETCH_USER_OFFER_ERROR,
  DELETE_OFFER,
} from './constants';

export const initialState = [];

/* eslint-disable default-case, no-param-reassign */
const panelPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_USER_OFFER:
        return initialState;
        break;
      case FETCH_USER_OFFER_SUCCESS:
        action.data.map(value => {
          draft.push(value);
        });
        break;
      case DELETE_OFFER:
        draft.splice(draft.findIndex(offer => offer.id === action.id), 1);
    }
  });

export default panelPageReducer;
