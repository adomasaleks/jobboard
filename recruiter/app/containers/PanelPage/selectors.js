import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the panelPage state domain
 */

const selectPanelPageDomain = state => state.panelPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PanelPage
 */

const makeSelectPanelPage = () =>
  createSelector(
    selectPanelPageDomain,
    substate => substate,
  );

export default makeSelectPanelPage;
export { selectPanelPageDomain };
