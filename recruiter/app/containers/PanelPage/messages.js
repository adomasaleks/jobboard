/*
 * PanelPage Messages
 *
 * This contains all the text for the PanelPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.PanelPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PanelPage container!',
  },
});
