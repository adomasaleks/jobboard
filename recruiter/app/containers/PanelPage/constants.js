/*
 *
 * PanelPage constants
 *
 */

export const DEFAULT_ACTION = 'app/PanelPage/DEFAULT_ACTION';

export const FETCH_USER_OFFER = 'app/PanelPage/FETCH_USER_OFFER';
export const FETCH_USER_OFFER_SUCCESS =
  'app/PanelPage/FETCH_USER_OFFER_SUCCESS';
export const FETCH_USER_OFFER_ERROR = 'app/PanelPage/FETCH_USER_OFFER_ERROR';

export const DELETE_OFFER = 'app/OfferPage/DELETE_OFFER';
export const DELETE_OFFER_SUCCESS = 'app/OfferPage/DELETE_OFFER_SUCCESS';
