import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { FETCH_USER_OFFER } from './constants';
import { fetchOffer } from 'containers/API/offer';
import { fetchOffersSuccess } from './actions';

function* fetchRecruiterOffer() {
  try {
    const res = yield call(fetchOffer);
    yield put(fetchOffersSuccess(res.data));
  } catch (e) {
    console.log(e);
  }
}

// Individual exports for testing
export default function* panelPageSaga() {
  yield takeLatest(FETCH_USER_OFFER, fetchRecruiterOffer);
}
