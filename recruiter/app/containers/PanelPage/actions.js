/*
 *
 * PanelPage actions
 *
 */

import { fetchDeleteOffer } from 'containers/API/offer';
import {
  FETCH_USER_OFFER,
  FETCH_USER_OFFER_SUCCESS,
  DELETE_OFFER,
} from './constants';

export function fetchPost() {
  return {
    type: FETCH_USER_OFFER,
  };
}
export function fetchOffersSuccess(data) {
  return {
    type: FETCH_USER_OFFER_SUCCESS,
    data,
  };
}
export function deleteOffer(id) {
  fetchDeleteOffer(id);
  return {
    type: DELETE_OFFER,
    id,
  };
}
