/**
 *
 * PanelPage
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import PanelTable from 'components/PanelTable';
import { fetchPost, deleteOffer } from './actions';
import makeSelectPanelPage from './selectors';
import reducer from './reducer';
import saga from './saga';

export function PanelPage({ fetchPost, panelPage, deleteOffer }) {
  useInjectReducer({ key: 'panelPage', reducer });
  useInjectSaga({ key: 'panelPage', saga });
  console.log(panelPage);
  useEffect(() => {
    fetchPost();
  }, []);

  return (
    <div>
      <PanelTable data={panelPage} deleteOffer={deleteOffer} />
    </div>
  );
}

PanelPage.propTypes = {
  fetchPost: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  panelPage: makeSelectPanelPage(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPost, deleteOffer }, dispatch);
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(PanelPage);
