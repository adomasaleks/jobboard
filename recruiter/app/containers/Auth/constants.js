/*
 *
 * Auth constants
 *
 */

export const DEFAULT_ACTION = 'app/Auth/DEFAULT_ACTION';

export const AUTH_SUCCESS = 'app/Auth/AUTH_SUCCESS';
export const AUTH_ERROR = 'app/Auth/AUTH_ERROR';
