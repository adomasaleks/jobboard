/**
 *
 * PrivateRoute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Redirect, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import makeSelectAuth from 'containers/Auth/selectors';
import isAuthenticated from 'utils/isAuthenticated';

export function PrivateRoute({ component: Component, auth, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated(auth.expTime) ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/', search: '?login=true' }} />
        )
      }
    />
  );
}

PrivateRoute.propTypes = {
  component: PropTypes.element,
  auth: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(PrivateRoute);
