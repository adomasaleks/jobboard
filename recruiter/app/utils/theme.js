import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#4D7298',
    },
    secondary: {
      main: '#77A6B6',
    },
    text: {
      primary: '#000000',
      secondary: '#F8F8F8',
    },
  },
  overrides: {
    MuiContainer: {
      maxWidthLg: {
        '@media (min-width: 1280px)': {
          maxWidth: 940,
        },
      },
    },
  },
});

export default theme;
