/*
 * HomeRegister Messages
 *
 * This contains all the text for the HomeRegister component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.HomeRegister';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the HomeRegister component!',
  },
});
