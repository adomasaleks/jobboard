/**
 *
 * HomeRegister
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import RegisterFrom from '../../containers/RegisterForm';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 120,
    paddingBottom: 140,
  },
  title: {
    marginBottom: 50,
  },
  subtitle: {
    marginBottom: 20,
    '&:last-of-type': {
      marginBottom: 50,
    },
  },
}));
function HomeRegister() {
  const classes = useStyles();
  return (
    <div id="HomeRegister">
      <Container className={classes.root}>
        <Typography variant="h4" className={classes.title}>
          Prisijunkite
        </Typography>
        <Typography variant="h6" className={classes.subtitle}>
          - NEMOKAMA. 1 mėn. neribotą skaičių skelbimų įkelkite nemokamai
        </Typography>
        <Typography variant="h6" className={classes.subtitle}>
          - TIKSLINGA. Per mėnesį platformoje apsilanko 30 000+ darbo
          ieškančiųjų
        </Typography>
        <Typography variant="h6" className={classes.subtitle}>
          - PAPRASTA. Darbo skelbimą įkelkite vos per kelias minutes
        </Typography>
        <RegisterFrom />
      </Container>
    </div>
  );
}

HomeRegister.propTypes = {};

export default HomeRegister;
