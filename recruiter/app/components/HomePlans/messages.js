/*
 * HomePlans Messages
 *
 * This contains all the text for the HomePlans component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.HomePlans';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the HomePlans component!',
  },
});
