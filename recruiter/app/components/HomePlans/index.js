/**
 *
 * HomePlans
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CheckIcon from '@material-ui/icons/Check';
import ColorButton from '../ColorButton';

const useStyles = makeStyles(() => ({
  root: {
    marginTop: 120,
    marginBottom: 130,
  },
  title: {
    fontWeight: 500,
    marginBottom: 50,
  },
  subtitle: {
    fontWeight: 500,
    marginLeft: 15,
  },
  container: {
    display: 'flex',
    marginBottom: 20,
  },
  button: {
    marginTop: 45,
  },
}));

const data = [
  {
    title: 'Nemokamas skelbimų kėlimas',
    ex: [
      'Limituotas skaičius darbo skelbimų',
      'Paminėjimas savaitiniame naujienlaiškyje',
      'Galimybė pasirinkti individualų skelbimų planą , bet kada',
    ],
    button: 'SKELBK SKELBIMUS NEMOKAMAI',
  },
  {
    title: 'Individualus skelbimų kėlimas',
    ex: [
      'Nelimituotas skaičius darbo skelbimų',
      'Paminėjimas savaitiniame naujienlaiškyje',
      'Skelbimų iškėlimas',
    ],
    button: 'SUŽINOK DAUGIAU',
  },
];

function HomePlans() {
  const classes = useStyles();
  return (
    <div id="HomePlans" className={classes.root}>
      <Container fixed>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={8}
        >
          {data.map(
            item => (
              <Grid item md={5} key={item.title}>
                <Typography variant="h5" className={classes.title}>
                  {item.title}
                </Typography>
                {item.ex.map(check => (
                  <div className={classes.container} key={check}>
                    <CheckIcon style={{ color: '#ECC30B' }} />
                    <Typography
                      variant="subtitle1"
                      component="span"
                      className={classes.subtitle}
                    >
                      {check}
                    </Typography>
                  </div>
                ))}
                <div className={classes.button}>
                  <ColorButton
                    message={item.button}
                    backgroundColor="#4D7298"
                  />
                </div>
              </Grid>
            ),
            {},
          )}
        </Grid>
      </Container>
    </div>
  );
}

HomePlans.propTypes = {};

export default HomePlans;
