/**
 *
 * Footer
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: '#77A6B6',
    paddingTop: 20,
    paddingBottom: 20,
  },
}));

function Footer() {
  const classes = useStyles();
  return (
    <div id="Footer" className={classes.root}>
      <Typography variant="subtitle2" color="textSecondary" align="center">
        © 2020 adresas.lt Saugomos visos teisės.
      </Typography>
    </div>
  );
}

Footer.propTypes = {};

export default Footer;
