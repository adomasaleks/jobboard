/**
 *
 * ColorButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Button from '@material-ui/core/Button';

function ColorButton({ message, backgroundColor, color, ...rest }) {
  return (
    <Button
      style={{
        backgroundColor,
        color: color === undefined ? '#FFFFFF' : color,
        maxHeight: 36,
      }}
      variant="contained"
      {...rest}
    >
      {message}
    </Button>
  );
}

ColorButton.propTypes = {
  message: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
};

export default ColorButton;
