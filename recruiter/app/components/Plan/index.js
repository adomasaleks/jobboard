/**
 *
 * Plan
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  container: {
    padding: theme.spacing(5, 3, 2, 3),
    backgroundColor: '#ffffff',
  },
  plan: {
    display: 'inline-block',
    padding: theme.spacing(1, 2, 1, 2),
    backgroundColor: '#56CCF2',
    color: '#ffffff',
    border: '1px solid #56CCF2',
    borderRadius: 100,
  },
  subtitle: {
    color: '#9B9B9B',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  body: {
    marginBottom: theme.spacing(3),
  },
  button: {
    color: '#ECC30B',
  },
}));

function Plan({ values }) {
  const classes = useStyles();
  console.log(values);
  return (
    <Grid container className={classes.container}>
      <Grid item md={6}>
        <Typography variant="h4">€ {values.cost}</Typography>
      </Grid>
      <Grid container item md={6} justify="flex-end">
        <Typography variant="body1" className={classes.plan}>
          {values.plan}
        </Typography>
      </Grid>
      <Grid item md={12}>
        <Typography variant="body2" className={classes.subtitle}>
          Per mėnesį
        </Typography>
      </Grid>
      <Grid item md={12} className={classes.body}>
        {values.body.map(item => (
          <div>
            <Typography variant="body1">{item}</Typography>
          </div>
        ))}
      </Grid>
      <Grid item md={12}>
        <Button className={classes.button}>Užsakyti</Button>
      </Grid>
    </Grid>
  );
}

Plan.propTypes = {};

export default Plan;
