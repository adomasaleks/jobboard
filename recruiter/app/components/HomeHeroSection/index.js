/**
 *
 * HomeHeroSection
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Filter1OutlinedIcon from '@material-ui/icons/Filter1Outlined';
import Filter2OutlinedIcon from '@material-ui/icons/Filter2Outlined';
import background from '../../images/landingHero.jpg';
import ColorButton from '../ColorButton';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    paddingTop: 120,
    paddingBottom: 90,
    background: `url(${background}) no-repeat`,
    backgroundSize: 'cover',
  },
  container: {
    position: 'relative',
    zIndex: 2,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    background:
      'linear-gradient(74.49deg, rgba(77, 114, 152, 0.5) 46.08%, rgba(248, 248, 248, 0.5) 92.95%);',
    zIndex: 1,
  },
  title: {
    marginBottom: 45,
  },
  subtitle: {
    marginBottom: 50,
  },
  choseContainer: {
    paddingLeft: 45,
  },
  icon: {
    marginRight: 30,
  },
}));
function HomeHeroSection() {
  const classes = useStyles();
  return (
    <div id="HomeHeroSection">
      <div className={classes.root}>
        {/* prettier-ignore */}
        <div className={classes.background}></div>
        <Container fixed className={classes.container}>
          <Typography
            variant="h3"
            component="h1"
            color="textSecondary"
            className={classes.title}
          >
            Darbuotojo reikia dabar?
          </Typography>
          <Typography
            variant="h5"
            component="h2"
            color="textSecondary"
            className={classes.subtitle}
          >
            Paprasta darbuotojų paieška ir įdarbinimas
          </Typography>
          <div className={classes.choseContainer}>
            <div style={{ marginBottom: 40 }}>
              <Filter1OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Susikurk skelbimą
              </Typography>
            </div>
            <div style={{ marginBottom: 100 }}>
              <Filter2OutlinedIcon className={classes.icon} />
              <Typography variant="h5" component="span" color="textSecondary">
                Išsirink kandidatus
              </Typography>
            </div>
            <ColorButton message="IŠBANDYK DABAR" backgroundColor="#ECC30B" />
          </div>
        </Container>
      </div>
    </div>
  );
}

HomeHeroSection.propTypes = {};

export default HomeHeroSection;
