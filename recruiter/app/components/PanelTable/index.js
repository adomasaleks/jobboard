/**
 *
 * PanelTable
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

//Material-ui
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import history from 'utils/history';

const useStyles = makeStyles(() => ({
  table: {
    minWidth: 650,
  },
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.secondary,
  },
}))(TableCell);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function PanelTable({ data, deleteOffer }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div id="PanelTable">
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead className={classes.tableHead}>
            <TableRow>
              <StyledTableCell>Pavadinimas</StyledTableCell>
              <StyledTableCell align="center">Kandidatūros</StyledTableCell>
              <StyledTableCell align="center">
                Peržiūrėti kandidatūras
              </StyledTableCell>
              <StyledTableCell align="center">Veiksmai</StyledTableCell>
              <StyledTableCell align="center">Statusas</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="center">{row.id}</TableCell>
                <TableCell align="center">
                  <IconButton onClick={handleClickOpen}>
                    <AddCircleOutlineIcon />
                  </IconButton>
                </TableCell>
                <TableCell align="center">
                  <IconButton
                    onClick={() => {
                      history.push(`/offer/${row.id}`);
                    }}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => {
                      deleteOffer(row.id);
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
                <TableCell align="center">
                  <Brightness1Icon
                    style={row.status ? { color: 'green' } : { color: 'red' }}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        maxWidth="md"
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {'Kandidatūros'}
        </DialogTitle>
        <DialogContent>
          <Table
            className={classes.table}
            size="small"
            aria-label="a dense table"
          >
            <TableHead>
              <TableRow>
                <TableCell>Vardas Pavardė</TableCell>
                <TableCell align="right">Paštas</TableCell>
                <TableCell align="right">Šalis</TableCell>
                <TableCell align="right">Telefono Numeris</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow key="name">
                <TableCell component="th" scope="row">
                  Vardenis pavardenis
                </TableCell>
                <TableCell align="right">vardas@pavarde.com</TableCell>
                <TableCell align="right">Ukraina</TableCell>
                <TableCell align="right">8363245</TableCell>
              </TableRow>
              <TableRow key="name">
                <TableCell component="th" scope="row">
                  Vardenis pavardenis
                </TableCell>
                <TableCell align="right">vardas@pavarde.com</TableCell>
                <TableCell align="right">Ukraina</TableCell>
                <TableCell align="right">8363245</TableCell>
              </TableRow>
              <TableRow key="name">
                <TableCell component="th" scope="row">
                  Vardenis pavardenis
                </TableCell>
                <TableCell align="right">vardas@pavarde.com</TableCell>
                <TableCell align="right">Ukraina</TableCell>
                <TableCell align="right">8363245</TableCell>
              </TableRow>
              <TableRow key="name">
                <TableCell component="th" scope="row">
                  Vardenis pavardenis
                </TableCell>
                <TableCell align="right">vardas@pavarde.com</TableCell>
                <TableCell align="right">Ukraina</TableCell>
                <TableCell align="right">8363245</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </DialogContent>
      </Dialog>
    </div>
  );
}

PanelTable.propTypes = {};

export default PanelTable;
