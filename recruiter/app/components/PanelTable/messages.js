/*
 * PanelTable Messages
 *
 * This contains all the text for the PanelTable component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PanelTable';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PanelTable component!',
  },
});
