/*
 * HomeContact Messages
 *
 * This contains all the text for the HomeContact component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.HomeContact';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the HomeContact component!',
  },
});
