/**
 *
 * HomeContact
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import background from '../../images/bg-for-contact.jpg';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    paddingTop: 120,
    paddingBottom: 120,
    background: `url(${background}) no-repeat`,
  },
  title: {
    fontWeight: 500,
    marginBottom: 70,
  },
  item: {
    display: 'flex',
    alignItems: 'center',
  },
  info: {
    paddingLeft: 15,
  },
}));
function HomeContact() {
  const classes = useStyles();
  return (
    <div id="HomeContact">
      <div className={classes.root}>
        <Container>
          <Typography
            variant="h4"
            color="textSecondary"
            className={classes.title}
          >
            Susisiekite
          </Typography>
          <Grid container>
            <Grid item md={6} className={classes.item}>
              <PhoneIcon style={{ color: '#fff' }} />
              <Typography
                variant="h5"
                color="textSecondary"
                component="span"
                className={classes.info}
              >
                +370 601 30 121
              </Typography>
            </Grid>
            <Grid item md={6} className={classes.item}>
              <MailIcon style={{ color: '#fff' }} />
              <Typography
                variant="h5"
                color="textSecondary"
                component="span"
                className={classes.info}
              >
                info@pastas.lt
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
}

HomeContact.propTypes = {};

export default HomeContact;
