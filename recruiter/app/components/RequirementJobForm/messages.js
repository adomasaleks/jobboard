/*
 * RequirementJobForm Messages
 *
 * This contains all the text for the RequirementJobForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RequirementJobForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RequirementJobForm component!',
  },
});
