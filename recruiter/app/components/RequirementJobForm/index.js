/**
 *
 * RequirementJobForm
 *
 */

import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles(() => ({
  title: {
    marginTop: 65,
    marginBottom: 35,
  },
  textFieldContainer: {
    marginBottom: 20,
  },
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonAdd: {
    width: 48,
    height: 48,
    backgroundColor: '#56CCF2',
    color: '#ffffff',
  },
  buttonDelete: {
    width: 48,
    height: 48,
    backgroundColor: '#E10050',
    color: '#ffffff',
  },
}));

function RequirementJobForm({ requirements, answers, actions }) {
  const classes = useStyles();

  const deleteAnswer = idx => {
    answers.splice(idx, 1);
    setAnswers([...answers]);
  };

  const addAnswer = () => {
    setAnswers([...answers, '']);
  };
  const updateAnswer = e => {
    const answerList = [...answers];
    answerList[e.target.id] = e.target.value;
    setAnswers(answerList);
  };
  return (
    <div>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <TextField
            fullWidth
            id="workRequirements"
            label="Reikalavimai"
            multiline
            rows={7}
            variant="outlined"
            value={requirements}
            onChange={e => {
              actions.updateTextField('workRequirements', e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.title}>
            Klausymai
          </Typography>
          {answers.map((val, idx) => (
            <Grid
              container
              justify="space-between"
              key={`${idx}-answer`}
              className={classes.textFieldContainer}
            >
              <Grid item md={11}>
                <TextField
                  fullWidth
                  id={`${idx}`}
                  label="Įveskite klausymą"
                  variant="outlined"
                  value={val}
                  onChange={e => {
                    actions.updateAnswer(e.target.id, e.target.value);
                  }}
                  InputLabelProps={{
                    className: classes.textField,
                  }}
                />
              </Grid>
              <Grid item md={1} className={classes.buttonContainer}>
                {idx < answers.length - 1 ? (
                  <IconButton
                    classes={{ root: classes.buttonDelete }}
                    onClick={() => {
                      actions.deleteAnswer(idx);
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                ) : (
                  <IconButton
                    classes={{ root: classes.buttonAdd }}
                    onClick={actions.addNewAnswer}
                  >
                    <AddIcon />
                  </IconButton>
                )}
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

RequirementJobForm.propTypes = {};

export default RequirementJobForm;
