/**
 *
 * OfferJobForm
 *
 */

import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import ColorButton from 'components/ColorButton';

const useStyles = makeStyles(() => ({
  textField: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  header: {
    marginTop: 40,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
}));

function OfferJobForm({ offer, actions }) {
  const classes = useStyles();
  const buttonChange = () => {
    actions.updateTextField('fullTime', !offer.fullTime);
  };
  return (
    <div>
      <Grid container spacing={6}>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="name"
            label="Pozicijos pavadinimas"
            variant="outlined"
            value={offer.name}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="areaOfWork"
            label="Darbo sritis"
            variant="outlined"
            value={offer.areaOfWork}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={12}>
          <TextField
            fullWidth
            id="workDescription"
            label="Darbo aprašymas"
            multiline
            rows={7}
            variant="outlined"
            value={offer.workDescription}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.header}>
            Atlyginimas
          </Typography>
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="salaryFrom"
            label="Nuo"
            variant="outlined"
            value={offer.salaryFrom}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  className={classes.textField}
                  position="end"
                  disableTypography
                >
                  €
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="salaryTo"
            label="Iki"
            variant="outlined"
            value={offer.salaryTo}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  className={classes.textField}
                  position="end"
                  disableTypography
                >
                  €
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="payment"
            select
            value={offer.payment}
            label="Užmokestis"
            variant="outlined"
            onChange={e => {
              actions.updateTextField('payment', e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          >
            {offer.payments.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="taxes"
            select
            value={offer.tax}
            label="Užmokestis"
            variant="outlined"
            onChange={e => {
              actions.updateTextField('tax', e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          >
            {offer.taxes.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.header}>
            Darbo vieta
          </Typography>
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="companyName"
            label="Įmonė"
            variant="outlined"
            value={offer.companyName}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="showCompanyName"
            label="Skelbime rodomas įmonės pavadinimas"
            variant="outlined"
            helperText="PATARIMAS: Įmonės pavadinimas, kuris bus rodomas viešai"
            value={offer.showCompanyName}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="city"
            label="Miestas"
            variant="outlined"
            value={offer.city}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="address"
            label="Adresas"
            variant="outlined"
            value={offer.address}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={6}>
        <Grid item md={12}>
          <Typography variant="h5" className={classes.header}>
            Darbas
          </Typography>
        </Grid>
        <Grid item md={6} className={classes.buttonContainer}>
          <ColorButton
            id="fullTime"
            message="Pilnas etatas"
            backgroundColor={
              offer.fullTime === true ? '#56CCF2' : 'transparent'
            }
            color={offer.fullTime === true ? undefined : '#4D7298'}
            onClick={buttonChange}
          />
          <ColorButton
            id="fullTime"
            message="ne Pilnas etatas"
            backgroundColor={
              offer.fullTime === true ? 'transparent' : '#56CCF2'
            }
            color={offer.fullTime === true ? '#4D7298' : undefined}
            onClick={buttonChange}
          />
        </Grid>
        <Grid item md={6}>
          <TextField
            fullWidth
            id="schedule"
            label="Darbo grafikas"
            variant="outlined"
            value={offer.schedule}
            onChange={e => {
              actions.updateTextField(e.target.id, e.target.value);
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}

OfferJobForm.propTypes = {};

export default OfferJobForm;
