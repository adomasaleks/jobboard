/*
 * OfferJobForm Messages
 *
 * This contains all the text for the OfferJobForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.OfferJobForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the OfferJobForm component!',
  },
});
