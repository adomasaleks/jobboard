/**
 *
 * Header
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  spacing: {
    marginRight: 100,
  },
  link: {
    color: '#000000',
    textDecoration: 'none',
  },
  buttonLink: {
    color: '#ffffff',
    textDecoration: 'none',
  },
}));

function Header({ isAuth }) {
  console.log(isAuth);
  const classes = useStyles();
  const mainHeader = (
    <Link
      to={{ pathname: '/', search: '?login=true' }}
      className={classes.link}
    >
      <Button color="primary" variant="contained">
        Prisijungti
      </Button>
    </Link>
  );

  const secondHeader = (
    <div>
      <Button className={classes.spacing}>
        <Link to="/panel" className={classes.link}>
          SKELBIMAI
        </Link>
      </Button>
      <Button className={classes.spacing}>
        <Link to="/profile" className={classes.link}>
          PROFILIS
        </Link>
      </Button>
      <Button className={classes.spacing}>
        <Link to="/plan" className={classes.link}>
          PLANAI
        </Link>
      </Button>
      <Button color="primary" variant="contained">
        <Link to="/offer" className={classes.buttonLink}>
          Sukurti skelbimą
        </Link>
      </Button>
    </div>
  );
  return (
    <div>
      <AppBar
        className={classes.root}
        position="static"
        style={{ backgroundColor: '#F8F8F8' }}
      >
        <Toolbar>{isAuth ? secondHeader : mainHeader}</Toolbar>
      </AppBar>
    </div>
  );
}

Header.propTypes = {
  isAuth: PropTypes.bool.isRequired,
};

export default Header;
