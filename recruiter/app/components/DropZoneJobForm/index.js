/**
 *
 * DropZoneJobForm
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import { DropzoneAreaBase } from 'material-ui-dropzone';
import Chip from '@material-ui/core/Chip';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  previewChip: {
    minWidth: 160,
    maxWidth: 210,
  },
  container: {
    margin: theme.spacing(1),
  },
}));

function DropZoneJobForm({ file, action, deleteFile }) {
  const classes = useStyles();
  console.log(file.length);

  const onChangeFile = fileUpload => {
    console.log(fileUpload[0].file.name);
    action(fileUpload);
  };

  const handleDelete = () => {
    deleteFile();
  };

  return (
    <div>
      <DropzoneAreaBase
        classes={{ root: classes.root }}
        filesLimit={1}
        dropzoneText="Užtempkite arba pridėkite naują logotipą"
        showAlerts={false}
        showPreviewsInDropzone={false}
        useChipsForPreview
        onAdd={fileObjs => {
          onChangeFile(fileObjs);
        }}
      />
      {file.length !== 0 && (
        <Chip
          label={file[0].file.name}
          variant="outlined"
          onDelete={handleDelete}
          classes={{ root: classes.container }}
        />
      )}
    </div>
  );
}

DropZoneJobForm.propTypes = {};

export default DropZoneJobForm;
