/*
 * DropZoneJobForm Messages
 *
 * This contains all the text for the DropZoneJobForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DropZoneJobForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DropZoneJobForm component!',
  },
});
